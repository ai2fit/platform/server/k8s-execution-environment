ARG JAVA_VERSION=17
ARG GRADLE_VERSION=7.6

FROM gradle:${GRADLE_VERSION}-jdk${JAVA_VERSION} AS build-stage

ARG APP_VERSION
ENV VERSION=${APP_VERSION}
ENV ORG_GRADLE_PROJECT_version=${APP_VERSION}

# Please note that repository credentials are set in the build-stage.
# It is secure to do so as ARG statements, cause they won't be included in the final image
ARG ORG_GRADLE_PROJECT_mavenUser
ARG ORG_GRADLE_PROJECT_mavenPass

WORKDIR /workspace/app

COPY gradle gradle
COPY build.gradle settings.gradle gradlew ./
COPY src src
COPY .git ./.git

RUN ./gradlew assemble
RUN mkdir -p build/libs/dependency && (cd build/libs/dependency; find ../ -name "*.jar" -exec jar -xf {} \;)

FROM amazoncorretto:17 AS production-stage
VOLUME /tmp
ARG DEPENDENCY=/workspace/app/build/libs/dependency
COPY --from=build-stage ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=build-stage ${DEPENDENCY}/META-INF /app/META-INF
COPY --from=build-stage ${DEPENDENCY}/BOOT-INF/classes /app
ENTRYPOINT ["java","-cp","app:app/lib/*","com.owc.singularity.server.ExecutionControllerApplication"]
