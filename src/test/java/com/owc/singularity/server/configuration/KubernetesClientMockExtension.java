package com.owc.singularity.server.configuration;

import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;


public class KubernetesClientMockExtension implements BeforeEachCallback, AfterEachCallback, ExtensionContext.Store.CloseableResource {

    @Override
    public void afterEach(ExtensionContext context) throws Exception {
        MyKubernetesMockServer.reset();
    }

    @Override
    public void beforeEach(ExtensionContext context) throws Exception {
        MyKubernetesMockServer.reset();
    }

    @Override
    public void close() throws Throwable {
        MyKubernetesMockServer.close();
    }
}
