package com.owc.singularity.server.configuration;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import io.fabric8.kubernetes.client.KubernetesClient;

@TestConfiguration
public class KubernetesMockClientConfiguration {

    @Bean
    @Primary
    KubernetesClient getKubernetesMockClient() {
        return MyKubernetesMockServer.getClient();
    }
}
