package com.owc.singularity.server.configuration;

import java.net.InetAddress;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.server.mock.KubernetesMixedDispatcher;
import io.fabric8.kubernetes.client.server.mock.KubernetesMockServer;
import io.fabric8.mockwebserver.Context;
import io.fabric8.mockwebserver.ServerRequest;
import io.fabric8.mockwebserver.ServerResponse;
import okhttp3.mockwebserver.MockWebServer;

public class MyKubernetesMockServer {

    private static KubernetesClient client = null;
    private static KubernetesMockServer mock = null;

    public static KubernetesClient getClient() {
        if (client == null) {
            final Map<ServerRequest, Queue<ServerResponse>> responses = new HashMap<>();
            mock = new KubernetesMockServer(new Context(), new MockWebServer(), responses, new KubernetesMixedDispatcher(responses, Collections.emptyList()),
                    true);
            mock.init(InetAddress.getLoopbackAddress(), 0);
            client = mock.createClient();
        }

        return client;
    }

    public static void reset() {
        if (mock != null)
            mock.reset();
    }

    static void close() {
        if (mock != null)
            mock.destroy();
        if (client != null)
            client.close();
    }
}
