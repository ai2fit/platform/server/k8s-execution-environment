package com.owc.singularity.server.service.log.elastic;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.apache.http.HttpHost;
import org.junit.jupiter.api.Test;

class ElasticTest {

    @Test
    void testParseNodeDefinitions() {
        String nodes = "localhost:9200,elasticsearch-master:9200";

        HttpHost[] hosts = new HttpHost[] { new HttpHost("localhost", 9200), new HttpHost("elasticsearch-master", 9200) };

        assertArrayEquals(hosts, Elastic.parseNodeDefinitions(nodes));
    }
}
