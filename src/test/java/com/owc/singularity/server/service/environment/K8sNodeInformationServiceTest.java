package com.owc.singularity.server.service.environment;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.owc.singularity.server.model.NodeInformation;
import com.owc.singularity.server.model.NodeResources;
import com.owc.singularity.server.model.QueryOptions;
import com.owc.singularity.server.service.exception.AccessRightException;
import com.owc.singularity.server.service.job.k8s.scheduler.K8sScheduler;


@SpringBootTest
class K8sNodeInformationServiceTest {

    @MockBean
    K8sScheduler scheduler;

    @Autowired
    K8sNodeInformationService nodeInformationService;

    @Nested
    class allTests {

        @Test
        void allNodesWithEmpty() throws Exception {
            given(scheduler.getNodeInformation()).willReturn(Stream.empty());

            assertEquals(0, nodeInformationService.all(null).count());
        }

        @Test
        void allNodesWithOneNode() throws Exception {
            given(scheduler.getNodeInformation()).willReturn(
                    Collections.singletonList(new NodeInformation().id("my-node").capacity(new NodeResources()).allocatable(new NodeResources())).stream());

            List<NodeInformation> nodes = nodeInformationService.all(null).collect(Collectors.toList());
            assertEquals(1, nodes.size());

            NodeInformation node = nodes.get(0);
            assertEquals("my-node", node.getId());
            assertNodeResources(node.getAllocatable(), null, null, null);
            assertNodeResources(node.getCapacity(), null, null, null);
        }

        @Test
        void allNodesWithOneNodePartlyDefined() throws Exception {
            given(scheduler.getNodeInformation()).willReturn(Collections
                    .singletonList(
                            new NodeInformation().id("my-node").capacity(new NodeResources().cpu("24").memory("1000000")).allocatable(new NodeResources()))
                    .stream());

            List<NodeInformation> nodes = nodeInformationService.all(null).collect(Collectors.toList());
            assertEquals(1, nodes.size());

            NodeInformation node = nodes.get(0);
            assertEquals("my-node", node.getId());
            assertNodeResources(node.getAllocatable(), null, null, null);
            assertNodeResources(node.getCapacity(), "24", "1000000", null);
        }

        @Test
        void allNodesWithOneNodeFullyDefined() throws Exception {
            given(scheduler.getNodeInformation()).willReturn(Collections.singletonList(new NodeInformation().id("my-node")
                    .capacity(new NodeResources().cpu("24").memory("1000000").storage("100"))
                    .allocatable(new NodeResources().cpu("244").memory("10000004").storage("1004"))).stream());

            List<NodeInformation> nodes = nodeInformationService.all(null).collect(Collectors.toList());
            assertEquals(1, nodes.size());

            NodeInformation node = nodes.get(0);
            assertEquals("my-node", node.getId());
            assertNodeResources(node.getAllocatable(), "244", "10000004", "1004");
            assertNodeResources(node.getCapacity(), "24", "1000000", "100");
        }

        private void assertNodeResources(NodeResources env, String cpu, String memory, String storage) {
            assertEquals(cpu, env.getCpu());
            assertEquals(memory, env.getMemory());
            assertEquals(storage, env.getStorage());
        }
    }

    @Nested
    class filterTests {

        @Test
        void cursor() throws AccessRightException, IOException {
            Stream<NodeInformation> toFilter = nodeInformationService.all(null);

            QueryOptions queryOptions = new QueryOptions();
            queryOptions.setCursor("test");
            assertThrows(IllegalArgumentException.class, () -> nodeInformationService.filter(null, queryOptions, toFilter));
        }

        @Test
        void filter() throws AccessRightException, IOException {
            Stream<NodeInformation> toFilter = nodeInformationService.all(null);

            QueryOptions queryOptions = new QueryOptions();
            queryOptions.setFilter(Collections.singletonList("test"));
            assertThrows(IllegalArgumentException.class, () -> {
                nodeInformationService.filter(null, queryOptions, toFilter);
            });
        }

        @Test
        void sort() throws AccessRightException, IOException {
            Stream<NodeInformation> toFilter = nodeInformationService.all(null);

            QueryOptions queryOptions = new QueryOptions();
            queryOptions.setSort(Collections.singletonList("test"));
            assertThrows(IllegalArgumentException.class, () -> nodeInformationService.filter(null, queryOptions, toFilter));
        }

        @Test
        void size() throws Exception {
            QueryOptions queryOptions = new QueryOptions();
            queryOptions.setSize(1);

            List<NodeInformation> info = new ArrayList<>();
            info.add(new NodeInformation().id("my-node").capacity(new NodeResources()).allocatable(new NodeResources()));
            info.add(new NodeInformation().id("my-node-2").capacity(new NodeResources()).allocatable(new NodeResources()));
            given(scheduler.getNodeInformation()).willReturn(info.stream());

            List<NodeInformation> result = nodeInformationService.filter(null, queryOptions, nodeInformationService.all(null)).collect(Collectors.toList());
            assertEquals(1, result.size());
            assertEquals("my-node", result.get(0).getId());
        }

        @Test
        void size0() throws Exception {
            QueryOptions queryOptions = new QueryOptions();
            queryOptions.setSize(0);

            List<NodeInformation> info = new ArrayList<>();
            info.add(new NodeInformation().id("my-node").capacity(new NodeResources()).allocatable(new NodeResources()));
            info.add(new NodeInformation().id("my-node-2").capacity(new NodeResources()).allocatable(new NodeResources()));
            given(scheduler.getNodeInformation()).willReturn(info.stream());

            List<NodeInformation> result = nodeInformationService.filter(null, queryOptions, nodeInformationService.all(null)).collect(Collectors.toList());
            assertEquals(0, result.size());
        }
    }

    @Nested
    class findByIdTests {

        @Test
        void idExists() throws Exception {
            NodeInformation nodeInformation = new NodeInformation().id("my-node").capacity(new NodeResources()).allocatable(new NodeResources());

            given(scheduler.getNodeInformation()).willReturn(Collections.singletonList(nodeInformation).stream());

            assertEquals(nodeInformation, nodeInformationService.findById(null, "my-node", nodeInformationService.all(null)));
        }

        @Test
        void idNotExists() throws Exception {
            NodeInformation nodeInformation = new NodeInformation().id("my-node").capacity(new NodeResources()).allocatable(new NodeResources());

            given(scheduler.getNodeInformation()).willReturn(Collections.singletonList(nodeInformation).stream());

            assertEquals(null, nodeInformationService.findById(null, "my-node-2", nodeInformationService.all(null)));
        }
    }
}
