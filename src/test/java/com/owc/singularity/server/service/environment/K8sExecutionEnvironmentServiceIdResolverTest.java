package com.owc.singularity.server.service.environment;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;

import java.io.IOException;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.owc.singularity.server.service.exception.AccessRightException;
import com.owc.singularity.server.service.job.k8s.scheduler.K8sScheduler;

@SpringBootTest(properties = { "environment.id=predefined" })
public class K8sExecutionEnvironmentServiceIdResolverTest {

    @MockBean
    K8sScheduler scheduler;

    @MockBean
    K8sNodeInformationService nodeInformationService;

    @Autowired
    K8sExecutionEnvironmentService executionEnvironmentService;

    @Test
    void withSuppliedId() throws AccessRightException, IOException {

        given(scheduler.getId()).willReturn("not-predefined");
        given(nodeInformationService.all(null)).willReturn(Stream.empty());

        assertEquals("predefined", executionEnvironmentService.self(null).getId());
    }
}
