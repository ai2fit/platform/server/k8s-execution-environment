package com.owc.singularity.server.service.environment;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.owc.singularity.server.model.ExecutionEnvironment;
import com.owc.singularity.server.model.NodeInformation;
import com.owc.singularity.server.model.NodeResources;
import com.owc.singularity.server.model.QueryOptions;
import com.owc.singularity.server.service.job.k8s.scheduler.K8sScheduler;

@SpringBootTest
class K8sExecutionEnvironmentServiceTest {

    @MockBean
    K8sScheduler scheduler;

    @MockBean
    K8sNodeInformationService nodeInformationService;

    @Autowired
    K8sExecutionEnvironmentService executionEnvironmentService;

    @Nested
    class allTests {

        @Test
        void allEnvironmentsReturnsSelf() throws Exception {
            String id = UUID.randomUUID().toString();
            given(scheduler.getId()).willReturn(id);

            List<NodeInformation> nodes = Collections
                    .singletonList(new NodeInformation().id("my-node").capacity(new NodeResources()).allocatable(new NodeResources()));
            given(nodeInformationService.all(null)).willReturn(nodes.stream());

            List<ExecutionEnvironment> result = executionEnvironmentService.all(null).collect(Collectors.toList());
            assertEquals(1, result.size());
            assertEquals(new ExecutionEnvironment().id(id).nodes(nodes), result.get(0));
        }
    }

    @Nested
    class filterTests {

        @Test
        void cursor() {
            QueryOptions queryOptions = new QueryOptions();
            queryOptions.setCursor("test");
            assertThrows(IllegalArgumentException.class, () -> executionEnvironmentService.filter(null, queryOptions));
        }

        @Test
        void filter() {
            QueryOptions queryOptions = new QueryOptions();
            queryOptions.setFilter(Collections.singletonList("test"));
            assertThrows(IllegalArgumentException.class, () -> executionEnvironmentService.filter(null, queryOptions));
        }

        @Test
        void sort() {
            QueryOptions queryOptions = new QueryOptions();
            queryOptions.setSort(Collections.singletonList("test"));
            assertThrows(IllegalArgumentException.class, () -> executionEnvironmentService.filter(null, queryOptions));
        }

        @Test
        void size() throws Exception {
            QueryOptions queryOptions = new QueryOptions();
            queryOptions.setSize(1);

            String id = UUID.randomUUID().toString();
            given(scheduler.getId()).willReturn(id);
            List<NodeInformation> nodes = Collections
                    .singletonList(new NodeInformation().id("my-node").capacity(new NodeResources()).allocatable(new NodeResources()));
            given(nodeInformationService.all(null)).willReturn(nodes.stream());

            List<ExecutionEnvironment> result = executionEnvironmentService.filter(null, queryOptions).collect(Collectors.toList());
            assertEquals(1, result.size());
            assertEquals(new ExecutionEnvironment().id(id).nodes(nodes), result.get(0));
        }

        @Test
        void size0() throws Exception {
            QueryOptions queryOptions = new QueryOptions();
            queryOptions.setSize(0);

            String id = UUID.randomUUID().toString();
            given(scheduler.getId()).willReturn(id);
            List<NodeInformation> nodes = Collections
                    .singletonList(new NodeInformation().id("my-node").capacity(new NodeResources()).allocatable(new NodeResources()));
            given(nodeInformationService.all(null)).willReturn(nodes.stream());

            List<ExecutionEnvironment> result = executionEnvironmentService.filter(null, queryOptions).collect(Collectors.toList());
            assertEquals(0, result.size());
        }
    }

    @Nested
    class selfTests {

        @Test
        void selfReturnsSelf() throws Exception {
            String id = UUID.randomUUID().toString();
            given(scheduler.getId()).willReturn(id);
            List<NodeInformation> nodes = Collections
                    .singletonList(new NodeInformation().id("my-node").capacity(new NodeResources()).allocatable(new NodeResources()));
            given(nodeInformationService.all(null)).willReturn(nodes.stream());

            ExecutionEnvironment result = executionEnvironmentService.self(null);
            assertEquals(result, new ExecutionEnvironment().id(id).nodes(nodes));
        }
    }

    @Nested
    class findByIdTests {

        @Test
        void idExists() throws Exception {
            String id = UUID.randomUUID().toString();
            given(scheduler.getId()).willReturn(id);
            List<NodeInformation> nodes = Collections
                    .singletonList(new NodeInformation().id("my-node").capacity(new NodeResources()).allocatable(new NodeResources()));
            given(nodeInformationService.all(null)).willReturn(nodes.stream());

            assertEquals(new ExecutionEnvironment().id(id).nodes(nodes), executionEnvironmentService.findById(null, id));
        }

        @Test
        void idIsSelf() throws Exception {
            String id = UUID.randomUUID().toString();
            given(scheduler.getId()).willReturn(id);
            List<NodeInformation> nodes = Collections
                    .singletonList(new NodeInformation().id("my-node").capacity(new NodeResources()).allocatable(new NodeResources()));
            given(nodeInformationService.all(null)).willReturn(nodes.stream());

            assertEquals(new ExecutionEnvironment().id(id).nodes(nodes), executionEnvironmentService.findById(null, "self"));
        }

        @Test
        void idNotExists() throws Exception {
            String id = UUID.randomUUID().toString();
            given(scheduler.getId()).willReturn(id);
            List<NodeInformation> nodes = Collections
                    .singletonList(new NodeInformation().id("my-node").capacity(new NodeResources()).allocatable(new NodeResources()));
            given(nodeInformationService.all(null)).willReturn(nodes.stream());

            assertEquals(null, executionEnvironmentService.findById(null, "test"));
        }
    }
}
