package com.owc.singularity.server.service.job.k8s.scheduler;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.nio.file.Path;
import java.time.Instant;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;

import com.owc.singularity.server.configuration.KubernetesMockClientConfiguration;
import com.owc.singularity.server.model.ExecutionContext;
import com.owc.singularity.server.model.Job;
import com.owc.singularity.server.model.JobConfiguration;
import com.owc.singularity.server.model.MountConfiguration;
import com.owc.singularity.server.model.NodeInformation;
import com.owc.singularity.server.model.NodeResources;

import io.fabric8.kubernetes.api.model.ConfigMapBuilder;
import io.fabric8.kubernetes.api.model.EnvVar;
import io.fabric8.kubernetes.api.model.EnvVarBuilder;
import io.fabric8.kubernetes.api.model.NodeBuilder;
import io.fabric8.kubernetes.api.model.ObjectMetaBuilder;
import io.fabric8.kubernetes.api.model.Quantity;
import io.fabric8.kubernetes.api.model.ResourceRequirements;
import io.fabric8.kubernetes.client.KubernetesClient;

@SpringBootTest
@Import(KubernetesMockClientConfiguration.class)
@TestPropertySource(properties = { "service.job.scheduler.k8s.mounts.secret.volume.path=/config",
        "service.job.scheduler.k8s.mounts.secret.volume.fileName=repository_config.json",
        "service.job.scheduler.singularity.java.opts:-XX:+UseZGC -XX:+AlwaysPreTouch -XX:+UseNUMA" })
class K8sSchedulerTest {

    String secretMountVolumePath = "/config";
    String repositoryConfigurationFileName = "repository_config.json";
    String javaOpts = "-XX:+UseZGC -XX:+AlwaysPreTouch -XX:+UseNUMA";

    @Autowired
    KubernetesClient client;

    @Autowired
    K8sScheduler scheduler;

    @Nested
    class GetNodeInformation {

        @Test
        void getAllNodesWithEmpty() throws Exception {
            assertEquals(0, scheduler.getNodeInformation().count());
        }

        @Test
        void getAllNodesWithOneNode() throws Exception {
            client.nodes().resource(new NodeBuilder().withMetadata(new ObjectMetaBuilder().withName("my-node").build()).build()).create();

            List<NodeInformation> nodes = scheduler.getNodeInformation().collect(Collectors.toList());
            assertEquals(1, nodes.size());

            NodeInformation node = nodes.get(0);
            assertEquals("my-node", node.getId());
            assertNodeResources(node.getAllocatable(), null, null, null);
            assertNodeResources(node.getCapacity(), null, null, null);
        }

        @Test
        void getAllNodesWithOneNodePartlyDefined() throws Exception {
            client.nodes()
                    .resource(new NodeBuilder().withNewMetadata()
                            .withName("my-node")
                            .and()
                            .withNewStatus()
                            .addToCapacity("cpu", new Quantity("24"))
                            .addToCapacity("memory", new Quantity("1000000"))
                            .and()
                            .build())
                    .create();

            List<NodeInformation> nodes = scheduler.getNodeInformation().collect(Collectors.toList());
            assertEquals(1, nodes.size());

            NodeInformation node = nodes.get(0);
            assertEquals("my-node", node.getId());
            assertNodeResources(node.getAllocatable(), null, null, null);
            assertNodeResources(node.getCapacity(), "24", "1000000", null);
        }

        @Test
        void getAllNodesWithOneNodeFullyDefined() throws Exception {
            client.nodes()
                    .resource(new NodeBuilder().withNewMetadata()
                            .withName("my-node")
                            .and()
                            .withNewStatus()
                            .addToCapacity("cpu", new Quantity("24"))
                            .addToCapacity("memory", new Quantity("1000000"))
                            .addToCapacity("ephemeral-storage", new Quantity("100"))
                            .addToAllocatable("cpu", new Quantity("244"))
                            .addToAllocatable("memory", new Quantity("10000004"))
                            .addToAllocatable("ephemeral-storage", new Quantity("1004"))
                            .and()
                            .build())
                    .create();

            List<NodeInformation> nodes = scheduler.getNodeInformation().collect(Collectors.toList());
            assertEquals(1, nodes.size());

            NodeInformation node = nodes.get(0);
            assertEquals("my-node", node.getId());
            assertNodeResources(node.getAllocatable(), "244", "10000004", "1004");
            assertNodeResources(node.getCapacity(), "24", "1000000", "100");
        }

        private void assertNodeResources(NodeResources env, String cpu, String memory, String storage) {
            assertEquals(cpu, env.getCpu());
            assertEquals(memory, env.getMemory());
            assertEquals(storage, env.getStorage());
        }
    }

    @Nested
    class SubmitJob {

        Map<String, String> repositoryMountOptions;

        @BeforeEach
        void setup() {
            repositoryMountOptions = new LinkedHashMap<>();
            repositoryMountOptions.put("remote", "https://my-test-address.com/something.git");
            repositoryMountOptions.put("auth.username", "PRIVATE-TOKEN");
            repositoryMountOptions.put("auth.password", "ASDalcalksalwqlfrs4i33");
        }

        @Test
        void withoutContext() {
            Job job = new Job().id(UUID.randomUUID().toString())
                    ._configuration(new JobConfiguration().pipelinePath("path"))
                    .name("name")
                    .description("desc")
                    .submittedAt(Instant.now());

            String jobId = job.getId();
            JobConfiguration jobConfiguration = job.getConfiguration();

            IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class, () -> scheduler.submit(null, jobId, jobConfiguration));
            assertEquals("Scheduling a job currently needs a complete context provided in the JobSubmissions configuration.", thrown.getMessage());
        }

        @Test
        void withoutMounts() {
            Job job = new Job().id(UUID.randomUUID().toString())
                    ._configuration(new JobConfiguration().pipelinePath("path").context(new ExecutionContext().mounts(Collections.emptyList())))
                    .name("name")
                    .description("desc")
                    .submittedAt(Instant.now());

            String jobId = job.getId();
            JobConfiguration jobConfiguration = job.getConfiguration();

            IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class, () -> scheduler.submit(null, jobId, jobConfiguration));
            assertEquals("Scheduling a job currently needs at least 1(!) MountConfiguration provided in the JobSubmissions configuration.",
                    thrown.getMessage());
        }

        @Test
        void withDefaultResources() {
            Job job = new Job().id(UUID.randomUUID().toString())
                    ._configuration(new JobConfiguration().pipelinePath("path")
                            .context(new ExecutionContext()
                                    .mounts(Collections.singletonList(new MountConfiguration().path("/repository").options(repositoryMountOptions)))))
                    .name("name")
                    .description("desc")
                    .submittedAt(Instant.now());

            assertDoesNotThrow(() -> scheduler.submit(null, job.getId(), job.getConfiguration()));
            io.fabric8.kubernetes.api.model.batch.v1.Job createdJob = scheduler.getRawJob(job.getId()).orElse(null);

            assertNotNull(createdJob);
            ResourceRequirements resources = createdJob.getSpec().getTemplate().getSpec().getContainers().get(0).getResources();
            assertEquals("2.0", resources.getLimits().get("cpu").toString());
            assertEquals("8Gi", resources.getLimits().get("memory").toString());
            assertEquals(null, resources.getLimits().get("ephemeral-storage"));
        }

        @Test
        void withoutNode() {
            Job job = new Job().id(UUID.randomUUID().toString())
                    ._configuration(new JobConfiguration().pipelinePath("path")
                            .context(new ExecutionContext().resources(new NodeResources().cpu("7").memory("1024Mi").storage("2048Mi"))
                                    .mounts(Collections.singletonList(new MountConfiguration().path("/repository").options(repositoryMountOptions)))))
                    .name("name")
                    .description("desc")
                    .submittedAt(Instant.now());

            assertDoesNotThrow(() -> scheduler.submit(null, job.getId(), job.getConfiguration()));
            io.fabric8.kubernetes.api.model.batch.v1.Job createdJob = scheduler.getRawJob(job.getId()).orElse(null);

            assertNotNull(createdJob);
            assertEquals(null, createdJob.getSpec().getTemplate().getSpec().getNodeSelector().get("kubernetes.io/hostname"));
        }

        @Test
        void withMissingNode() {

            client.nodes().resource(new NodeBuilder().withMetadata(new ObjectMetaBuilder().withName("my-node-2").build()).build()).create();

            Job job = new Job().id(UUID.randomUUID().toString())
                    ._configuration(new JobConfiguration().pipelinePath("path")
                            .context(new ExecutionContext().resources(new NodeResources().cpu("7").memory("1024Mi").storage("2048Mi"))
                                    .nodeId("my-node")
                                    .mounts(Collections.singletonList(new MountConfiguration().path("/repository").options(repositoryMountOptions)))))
                    .name("name")
                    .description("desc")
                    .submittedAt(Instant.now());

            String jobId = job.getId();
            JobConfiguration jobConfiguration = job.getConfiguration();

            IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class, () -> scheduler.submit(null, jobId, jobConfiguration));
            assertEquals("Scheduling of job: " + jobId + " can't proceed since the specified node: my-node is not available. Available nodes are: [my-node-2]",
                    thrown.getMessage());
        }

        @Test
        void submitFullyDefined() {

            client.nodes().resource(new NodeBuilder().withMetadata(new ObjectMetaBuilder().withName("my-node").build()).build()).create();

            Job job = new Job().id(UUID.randomUUID().toString())
                    ._configuration(new JobConfiguration().pipelinePath("path")
                            .context(new ExecutionContext().resources(new NodeResources().cpu("7").memory("1024Mi").storage("2048Mi"))
                                    .nodeId("my-node")
                                    .mounts(Collections.singletonList(new MountConfiguration().path("/repository").options(repositoryMountOptions)))))
                    .name("name")
                    .description("desc")
                    .submittedAt(Instant.now());

            assertDoesNotThrow(() -> scheduler.submit(null, job.getId(), job.getConfiguration()));
            io.fabric8.kubernetes.api.model.batch.v1.Job createdJob = scheduler.getRawJob(job.getId()).orElse(null);

            assertNotNull(createdJob);
            assertEquals("my-node", createdJob.getSpec().getTemplate().getSpec().getNodeSelector().get("kubernetes.io/hostname"));

            ResourceRequirements resources = createdJob.getSpec().getTemplate().getSpec().getContainers().get(0).getResources();
            assertEquals("7.0", resources.getLimits().get("cpu").toString());
            assertEquals("1024Mi", resources.getLimits().get("memory").toString());
            assertEquals("2048Mi", resources.getLimits().get("ephemeral-storage").toString());
        }

        @Nested
        class EnvironmentConfiguration {

            @Test
            void withDefaultEnv() {

                String pipelinePath = "path";
                String repositoryMountPath = "/repository";
                String repositoryConfigPath = Path.of(secretMountVolumePath, repositoryConfigurationFileName).toAbsolutePath().toString();
                String logLevel = "INFO";

                Job job = new Job().id(UUID.randomUUID().toString())
                        ._configuration(new JobConfiguration().pipelinePath(pipelinePath)
                                .context(new ExecutionContext().logLevel(logLevel)
                                        .mounts(Collections.singletonList(new MountConfiguration().path(repositoryMountPath).options(repositoryMountOptions)))))
                        .name("name")
                        .submittedAt(Instant.now());

                assertDoesNotThrow(() -> scheduler.submit(null, job.getId(), job.getConfiguration()));
                io.fabric8.kubernetes.api.model.batch.v1.Job createdJob = scheduler.getRawJob(job.getId()).orElse(null);

                assertNotNull(createdJob);
                String cpuLimits = createdJob.getSpec()
                        .getTemplate()
                        .getSpec()
                        .getContainers()
                        .get(0)
                        .getResources()
                        .getLimits()
                        .get("cpu")
                        .toString()
                        .split("\\.")[0];

                List<EnvVar> envs = createdJob.getSpec().getTemplate().getSpec().getContainers().get(0).getEnv();

                assertThat(envs,
                        containsInAnyOrder(buildEnv("SINGULARITY_PIPELINE_PATH", pipelinePath), buildEnv("AI2FIT_PIPELINE_PATH", pipelinePath),
                                buildEnv("SINGULARITY_REPOSITORY_CONFIG_FILE_PATH", repositoryConfigPath),
                                buildEnv("AI2FIT_REPOSITORY_CONFIG_PATH", repositoryConfigPath), buildEnv("SINGULARITY_LOG_LEVEL", logLevel),
                                buildEnv("AI2FIT_LOG_LEVEL", logLevel), buildEnv("JAVA_OPTS", "-XX:ActiveProcessorCount=" + cpuLimits + " " + javaOpts)));
            }

            @Test
            void withEngineVariables() {

                String pipelinePath = "path";
                String repositoryMountPath = "/repository";
                String repositoryConfigPath = Path.of(secretMountVolumePath, repositoryConfigurationFileName).toAbsolutePath().toString();
                String logLevel = "INFO";
                Map<String, String> variables = new HashMap<>();
                variables.put("Variable1", "test1");
                variables.put("Variable2", "test2");

                Job job = new Job().id(UUID.randomUUID().toString())
                        ._configuration(new JobConfiguration().pipelinePath(pipelinePath)
                                .context(new ExecutionContext().logLevel(logLevel)
                                        .variables(variables)
                                        .mounts(Collections.singletonList(new MountConfiguration().path(repositoryMountPath).options(repositoryMountOptions)))))
                        .name("name")
                        .submittedAt(Instant.now());

                assertDoesNotThrow(() -> scheduler.submit(null, job.getId(), job.getConfiguration()));
                io.fabric8.kubernetes.api.model.batch.v1.Job createdJob = scheduler.getRawJob(job.getId()).orElse(null);

                assertNotNull(createdJob);
                String cpuLimits = createdJob.getSpec()
                        .getTemplate()
                        .getSpec()
                        .getContainers()
                        .get(0)
                        .getResources()
                        .getLimits()
                        .get("cpu")
                        .toString()
                        .split("\\.")[0];

                List<EnvVar> envs = createdJob.getSpec().getTemplate().getSpec().getContainers().get(0).getEnv();

                assertThat(envs,
                        hasItems(buildEnv("SINGULARITY_PIPELINE_PATH", pipelinePath), buildEnv("AI2FIT_PIPELINE_PATH", pipelinePath),
                                buildEnv("SINGULARITY_REPOSITORY_CONFIG_FILE_PATH", repositoryConfigPath),
                                buildEnv("AI2FIT_REPOSITORY_CONFIG_PATH", repositoryConfigPath), buildEnv("SINGULARITY_LOG_LEVEL", logLevel),
                                buildEnv("AI2FIT_LOG_LEVEL", logLevel), buildEnv("JAVA_OPTS", "-XX:ActiveProcessorCount=" + cpuLimits + " " + javaOpts)));

                for (Entry<String, String> entry : variables.entrySet()) {
                    assertThat(envs, hasItems(buildEnv("AI2FIT_VARIABLE_" + entry.getKey(), entry.getValue())));
                }
            }

            private EnvVar buildEnv(String name, String value) {
                return new EnvVarBuilder().withName(name).withValue(value).build();
            }
        }
    }

    @Nested
    class GetId {

        @Test
        void getIdWithoutDefined() throws Exception {
            assertFalse(scheduler.getId().isBlank());
        }

        @Test
        void stableAfterFirstCall() throws Exception {
            String id = scheduler.getId();
            assertFalse(id.isBlank());

            assertEquals(id, scheduler.getId());
        }

        @Test
        void getIdWithPredefined() throws Exception {
            String id = UUID.randomUUID().toString();

            client.configMaps()
                    .inNamespace("singularity")
                    .resource(new ConfigMapBuilder().withNewMetadata().withName("cluster-info").endMetadata().addToData(Map.of("cluster-name", id)).build())
                    .create();

            assertEquals(id, scheduler.getId());
        }
    }
}
