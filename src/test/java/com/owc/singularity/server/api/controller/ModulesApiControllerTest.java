package com.owc.singularity.server.api.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.nio.charset.StandardCharsets;
import java.util.Collections;

import org.bson.types.ObjectId;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.AdditionalMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import com.owc.singularity.server.model.QueryOptions;
import com.owc.singularity.server.model.QueryOptions.Direction;
import com.owc.singularity.server.model.module.ModuleInfo;
import com.owc.singularity.server.service.environment.LocalStorageModuleService;

@WebMvcTest(controllers = ModulesApiController.class)
public class ModulesApiControllerTest {

    private final static MediaType JSON = MediaType.valueOf("application/json");

    @Autowired
    MockMvc mockMvc;

    @MockBean
    LocalStorageModuleService moduleService;

    @Nested
    class getAll {

        public static final String getAllUrl = "/modules";

        @Test
        void withoutFilter() throws Exception {
            ModuleInfo result = new ModuleInfo();
            result.setObjectId(new ObjectId());

            given(moduleService.filter(any(), any())).willReturn(Collections.singletonList(result).stream());

            ResultActions getResponse = mockMvc.perform(get(getAllUrl).accept(JSON));
            getResponse.andExpect(status().isOk())
                    .andExpect(content().contentTypeCompatibleWith(JSON))
                    .andExpect(content().encoding(StandardCharsets.UTF_8))
                    .andExpect(content().json("[{\"_id\":" + result.getObjectId().toString()
                            + ",\"name\":null,\"version\":null,\"vendor\":null,\"requiredEngineVersion\":null,\"moduleKey\":null,\"moduleDependencies\":null,\"filePath\":null, \"uploadedAt\":null}]"));
        }

        @Test
        void passesAdditionalOptions() throws Exception {
            ModuleInfo result = new ModuleInfo();
            result.setObjectId(new ObjectId());

            QueryOptions options = new QueryOptions(0, Collections.singletonList("test"), Collections.singletonList("test2"), "cursor", Direction.After);

            given(moduleService.filter(any(), eq(options))).willReturn(Collections.singletonList(result).stream());

            ResultActions getResponse = mockMvc.perform(get(getAllUrl).queryParam("size", "0")
                    .queryParam("sort", "test")
                    .queryParam("filter", "test2")
                    .queryParam("cursor", "cursor")
                    .queryParam("direction", "after")
                    .accept(JSON));
            getResponse.andExpect(status().isOk())
                    .andExpect(content().contentTypeCompatibleWith(JSON))
                    .andExpect(content().encoding(StandardCharsets.UTF_8))
                    .andExpect(content().json("[{\"_id\":" + result.getObjectId().toString()
                            + ",\"name\":null,\"version\":null,\"vendor\":null,\"requiredEngineVersion\":null,\"moduleKey\":null,\"moduleDependencies\":null,\"filePath\":null, \"uploadedAt\":null}]"));
        }
    }

    @Nested
    class getModuleById {

        public static final String getByIdUrl = "/modules/{moduleId}";

        @Test
        void withMatchingId() throws Exception {
            ObjectId id = new ObjectId();
            ModuleInfo result = new ModuleInfo();
            result.setObjectId(id);

            given(moduleService.findById(any(), any())).willReturn(null);
            given(moduleService.findById(any(), eq(id.toString()))).willReturn(result);

            ResultActions getResponse = mockMvc.perform(get(getByIdUrl, id).accept(JSON));
            getResponse.andExpect(status().isOk())
                    .andExpect(content().contentTypeCompatibleWith(JSON))
                    .andExpect(content().encoding(StandardCharsets.UTF_8))
                    .andExpect(content().json("{\"_id\":" + result.getObjectId().toString()
                            + ",\"name\":null,\"version\":null,\"vendor\":null,\"requiredEngineVersion\":null,\"moduleKey\":null,\"moduleDependencies\":null,\"filePath\":null, \"uploadedAt\":null}"));
        }

        @Test
        void withoutMatchingId() throws Exception {
            ObjectId id = new ObjectId();
            ModuleInfo result = new ModuleInfo();
            result.setObjectId(id);

            given(moduleService.findById(any(), any())).willReturn(null);
            given(moduleService.findById(any(), eq(id.toString()))).willReturn(result);

            ResultActions getResponse = mockMvc.perform(get(getByIdUrl, "test").accept(JSON));
            getResponse.andExpect(status().isNotFound());
        }
    }

    @Nested
    class downloadModuleById {

        public static final String downloadByIdUrl = "/modules/{moduleId}/download";

        @ParameterizedTest
        @ValueSource(strings = { "application/java-archive", "application/octet-stream" })
        void withMatchingId(String contentType) throws Exception {
            File file = File.createTempFile("module", "jar");
            file.deleteOnExit();
            String fileContent = "I'm test content.";
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
                writer.write(fileContent);
            }

            given(moduleService.download(any(), any())).willReturn(null);
            given(moduleService.download(any(), eq("test"))).willReturn(file);

            ResultActions getResponse = mockMvc.perform(get(downloadByIdUrl, "test").accept(contentType));
            getResponse.andExpect(status().isOk())
                    .andExpect(content().contentTypeCompatibleWith(contentType))
                    .andExpect(header().string(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getName() + "\""))
                    .andExpect(header().string(HttpHeaders.CONTENT_LENGTH, file.length() + ""))
                    .andExpect(content().bytes(fileContent.getBytes()));
        }

        @ParameterizedTest
        @ValueSource(strings = { "application/java-archive", "application/octet-stream" })
        void withoutMatchingId(String contentType) throws Exception {
            File file = File.createTempFile("module", "jar");
            file.deleteOnExit();

            given(moduleService.download(any(), any())).willReturn(null);
            given(moduleService.download(any(), eq("asdfasddasdasd"))).willReturn(file);

            ResultActions getResponse = mockMvc.perform(get(downloadByIdUrl, "test").accept(contentType));
            getResponse.andExpect(status().isNotFound());
        }
    }

    @Nested
    class uploadModule {

        public static final String uploadUrl = "/modules";

        @ParameterizedTest
        @ValueSource(strings = { "application/java-archive", "application/octet-stream" })
        void works(String contentType) throws Exception {
            String fileContent = "I'm test content.";

            ModuleInfo result = new ModuleInfo();
            result.setObjectId(new ObjectId());

            given(moduleService.upload(any(), any())).willReturn(result);

            ResultActions postResponse = mockMvc.perform(post(uploadUrl).content(fileContent.getBytes()).accept("application/json").contentType(contentType));
            postResponse.andExpect(status().isOk())
                    .andExpect(content().contentTypeCompatibleWith(JSON))
                    .andExpect(content().encoding(StandardCharsets.UTF_8))
                    .andExpect(content().json("{\"_id\":" + result.getObjectId().toString()
                            + ",\"name\":null,\"version\":null,\"vendor\":null,\"requiredEngineVersion\":null,\"moduleKey\":null,\"moduleDependencies\":null,\"filePath\":null, \"uploadedAt\":null}"));
        }
    }

    @Nested
    class deleteModuleById {

        public static final String deleteByIdUrl = "/modules/{moduleId}";

        @Test
        void withMatchingId() throws Exception {
            ModuleInfo result = new ModuleInfo();
            result.setObjectId(new ObjectId());

            doThrow(new IllegalArgumentException()).when(moduleService).deleteById(any(), AdditionalMatchers.not(eq(result.getObjectId().toString())));

            ResultActions deleteResponse = mockMvc.perform(delete(deleteByIdUrl, result.getObjectId()).accept("application/json"));
            deleteResponse.andExpect(status().is(204));
        }

        @Test
        void withoutMatchingId() throws Exception {
            ModuleInfo result = new ModuleInfo();
            result.setObjectId(new ObjectId());

            doThrow(new IllegalArgumentException()).when(moduleService).deleteById(any(), AdditionalMatchers.not(eq(result.getObjectId().toString())));

            ResultActions deleteResponse = mockMvc.perform(delete(deleteByIdUrl, "test").accept("application/json"));
            deleteResponse.andExpect(status().isNotFound());
        }
    }
}
