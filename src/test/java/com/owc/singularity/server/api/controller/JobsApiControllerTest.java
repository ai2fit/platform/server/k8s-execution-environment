package com.owc.singularity.server.api.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.UUID;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.owc.singularity.server.api.JobsApi;
import com.owc.singularity.server.model.Job;
import com.owc.singularity.server.model.JobConfiguration;
import com.owc.singularity.server.model.QueryOptions;
import com.owc.singularity.server.model.QueryOptions.Direction;
import com.owc.singularity.server.service.JobService;
import com.owc.singularity.server.service.LogService;

@WebMvcTest(controllers = JobsApiController.class)
public class JobsApiControllerTest {

    private final static MediaType JSON = MediaType.valueOf("application/json");
    private final static MediaType TEXT = MediaType.valueOf("text/plain");

    @Autowired
    MockMvc mockMvc;

    @MockBean
    JobService jobService;

    @MockBean
    LogService logService;

    @Nested
    class JobsTests {

        @Nested
        class getAll {

            public static final String getAllUrl = "/jobs";

            @Test
            void withoutFilter() throws Exception {
                given(jobService.filter(any(), any())).willReturn(Collections.singletonList(new Job().id("test")).stream());

                ResultActions getResponse = mockMvc.perform(get(getAllUrl).accept(JSON));
                getResponse.andExpect(status().isOk())
                        .andExpect(content().contentTypeCompatibleWith(JSON))
                        .andExpect(content().encoding(StandardCharsets.UTF_8))
                        .andExpect(content().json(
                                "[{\"configuration\":null,\"id\":\"test\",\"name\":null,\"userId\":null,\"description\":null,\"submittedAt\":null,\"executions\":null}]"));
            }

            @Test
            void passesAdditionalOptions() throws Exception {

                QueryOptions options = new QueryOptions(0, Collections.singletonList("test"), Collections.singletonList("test2"), "cursor", Direction.After);

                given(jobService.filter(any(), eq(options))).willReturn(Collections.singletonList(new Job().id("test")).stream());

                ResultActions getResponse = mockMvc.perform(get(getAllUrl).queryParam("size", "0")
                        .queryParam("sort", "test")
                        .queryParam("filter", "test2")
                        .queryParam("cursor", "cursor")
                        .queryParam("direction", "after")
                        .accept(JSON));
                getResponse.andExpect(status().isOk())
                        .andExpect(content().contentTypeCompatibleWith(JSON))
                        .andExpect(content().encoding(StandardCharsets.UTF_8))
                        .andExpect(content().json(
                                "[{\"configuration\":null,\"id\":\"test\",\"name\":null,\"userId\":null,\"description\":null,\"submittedAt\":null,\"executions\":null}]"));
            }
        }

        @Nested
        class getJobById {

            public static final String getByIdUrl = "/jobs/{jobId}";

            @Test
            void withMatchingId() throws Exception {
                String id = UUID.randomUUID().toString();

                given(jobService.findById(any(), any())).willReturn(null);
                given(jobService.findById(any(), eq(id))).willReturn(new Job().id(id));

                ResultActions getResponse = mockMvc.perform(get(getByIdUrl, id).accept(JSON));
                getResponse.andExpect(status().isOk())
                        .andExpect(content().contentTypeCompatibleWith(JSON))
                        .andExpect(content().encoding(StandardCharsets.UTF_8))
                        .andExpect(content().json("{\"configuration\":null,\"id\":\"" + id
                                + "\",\"name\":null,\"userId\":null,\"description\":null,\"submittedAt\":null,\"executions\":null}"));
            }

            @Test
            void withoutMatchingId() throws Exception {
                String id = UUID.randomUUID().toString();

                given(jobService.findById(any(), any())).willReturn(null);
                given(jobService.findById(any(), eq(id))).willReturn(new Job().id(id));

                ResultActions getResponse = mockMvc.perform(get(getByIdUrl, "test").accept(JSON));
                getResponse.andExpect(status().isNotFound());
            }
        }

        @Nested
        class getJobExecutionLogById {

            public static final String getJobLogByIdUrl = "/jobs/{jobId}/logs";

            @Test
            void withMatchingId() throws Exception {
                String jobId = UUID.randomUUID().toString();
                String executionId = UUID.randomUUID().toString();

                given(logService.getLogsForExecution(any(), eq(jobId), eq(null), eq(null), eq(null))).willReturn("This is my log line.\n");

                ResultActions getResponse = mockMvc.perform(get(getJobLogByIdUrl, jobId, executionId).accept(TEXT));
                getResponse.andExpect(status().isOk())
                        .andExpect(content().contentTypeCompatibleWith(TEXT))
                        .andExpect(content().encoding(StandardCharsets.UTF_8))
                        .andExpect(content().string("This is my log line.\n"));
            }

            @Test
            void withoutMatchingJobId() throws Exception {
                String jobId = UUID.randomUUID().toString();
                String executionId = UUID.randomUUID().toString();

                given(logService.getLogsForExecution(any(), eq(jobId), eq(null), eq(null), eq(null))).willReturn("This is my log line.\n");

                ResultActions getResponse = mockMvc.perform(get(getJobLogByIdUrl, "test", executionId).accept(TEXT));
                getResponse.andExpect(status().isOk()).andExpect(content().string(""));
            }

            @Test
            void passesAdditionalOptions() throws Exception {
                String jobId = UUID.randomUUID().toString();
                String executionId = UUID.randomUUID().toString();

                given(logService.getLogsForExecution(any(), eq(jobId), eq("filter"), eq(20), eq(true))).willReturn("This is my log line.\n");

                ResultActions getResponse = mockMvc.perform(get(getJobLogByIdUrl, jobId, executionId).queryParam("filter", "filter")
                        .queryParam("lines", "20")
                        .queryParam("includeSourceTimestamps", "true")
                        .accept(TEXT));
                getResponse.andExpect(status().isOk())
                        .andExpect(content().contentTypeCompatibleWith(TEXT))
                        .andExpect(content().encoding(StandardCharsets.UTF_8))
                        .andExpect(content().string("This is my log line.\n"));
            }
        }

        @Nested
        class stopJobById {

            public static final String stopJobByIdUrl = "/jobs/{jobId}";

            @Test
            void withMatchingId() throws Exception {
                String jobId = UUID.randomUUID().toString();

                ResultActions getResponse = mockMvc.perform(delete(stopJobByIdUrl, jobId).accept(JSON));
                getResponse.andExpect(status().is(204));
            }

            // TODO: Test purposely fails since behavior is not yet implemented
            @Test
            @Disabled("Test purposely fails since behavior is not yet implemented")
            void withMatchingIdAlreadyStopped() throws Exception {
                String jobId = UUID.randomUUID().toString();

                ResultActions getResponse = mockMvc.perform(delete(stopJobByIdUrl, jobId).accept(JSON));
                getResponse.andExpect(status().isOk());
            }

            // TODO: Test purposely fails since behavior is not yet implemented
            @Test
            @Disabled("Test purposely fails since behavior is not yet implemented")
            void withoutMatchingId() throws Exception {
                // String jobId = UUID.randomUUID().toString();

                ResultActions getResponse = mockMvc.perform(delete(stopJobByIdUrl, "test").accept(JSON));
                getResponse.andExpect(status().is(404));
            }
        }

        @Nested
        class submitJob {

            public static final String submitJobUrl = "/jobs";

            @Test
            void success() throws Exception {
                String jobId = UUID.randomUUID().toString();

                JobConfiguration configuration = new JobConfiguration().pipelinePath("path");
                JobsApi.JobSubmission submission = new JobsApi.JobSubmission().name("name")
                        .description("description")
                        .configuration(new JobConfiguration().pipelinePath("path"));
                Job createdJob = new Job().id(jobId).name(submission.getName()).description(submission.getDescription())._configuration(configuration);

                ObjectMapper mapper = new ObjectMapper();

                given(jobService.submit(any(), eq(submission))).willReturn(createdJob);

                ResultActions postResponse = mockMvc.perform(post(submitJobUrl).content(mapper.writeValueAsString(submission)).contentType(JSON).accept(JSON));
                postResponse.andExpect(status().is(201))
                        .andExpect(content().contentTypeCompatibleWith(JSON))
                        .andExpect(content().encoding(StandardCharsets.UTF_8))
                        .andExpect(content().json(mapper.writeValueAsString(createdJob)));
            }
        }
    }
}
