package com.owc.singularity.server.api.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import com.owc.singularity.server.model.ExecutionEnvironment;
import com.owc.singularity.server.model.NodeInformation;
import com.owc.singularity.server.model.QueryOptions;
import com.owc.singularity.server.model.QueryOptions.Direction;
import com.owc.singularity.server.service.ExecutionEnvironmentService;
import com.owc.singularity.server.service.NodeInformationService;

@WebMvcTest(controllers = EnvironmentsApiController.class)
public class EnvironmentsApiControllerTest {

    private final static MediaType JSON = MediaType.valueOf("application/json");

    @Autowired
    MockMvc mockMvc;

    @MockBean
    NodeInformationService nodeInformationService;

    @MockBean
    ExecutionEnvironmentService executionEnvironmentService;

    @Nested
    class getAll {

        public static final String getAllUrl = "/environments";

        @Test
        void withoutFilter() throws Exception {
            given(executionEnvironmentService.filter(any(), any())).willReturn(
                    Collections.singletonList(new ExecutionEnvironment().id("id").nodes(Collections.singletonList(new NodeInformation().id("test")))).stream());

            ResultActions getResponse = mockMvc.perform(get(getAllUrl).accept(JSON));
            getResponse.andExpect(status().isOk())
                    .andExpect(content().contentTypeCompatibleWith(JSON))
                    .andExpect(content().encoding(StandardCharsets.UTF_8))
                    .andExpect(content().json("[{\"id\":\"id\",\"nodes\":[{\"id\":\"test\",\"capacity\":null,\"allocatable\":null}]}]"));
        }

        @Test
        void passesAdditionalOptions() throws Exception {

            QueryOptions options = new QueryOptions(0, Collections.singletonList("test"), Collections.singletonList("test2"), "cursor", Direction.After);

            given(executionEnvironmentService.filter(any(), eq(options))).willReturn(
                    Collections.singletonList(new ExecutionEnvironment().id("id").nodes(Collections.singletonList(new NodeInformation().id("test")))).stream());

            ResultActions getResponse = mockMvc.perform(get(getAllUrl).queryParam("size", "0")
                    .queryParam("sort", "test")
                    .queryParam("filter", "test2")
                    .queryParam("cursor", "cursor")
                    .queryParam("direction", "after")
                    .accept(JSON));
            getResponse.andExpect(status().isOk())
                    .andExpect(content().contentTypeCompatibleWith(JSON))
                    .andExpect(content().encoding(StandardCharsets.UTF_8))
                    .andExpect(content().json("[{\"id\":\"id\",\"nodes\":[{\"id\":\"test\",\"capacity\":null,\"allocatable\":null}]}]"));
        }
    }

    @Nested
    class getById {

        public static final String getByIdUrl = "/environments/{environmentId}";

        @Test
        void withMatchingId() throws Exception {
            String id = UUID.randomUUID().toString();

            given(executionEnvironmentService.findById(any(), any())).willReturn(null);
            given(executionEnvironmentService.findById(any(), eq(id))).willReturn(new ExecutionEnvironment().id(id));

            ResultActions getResponse = mockMvc.perform(get(getByIdUrl, id).accept(JSON));
            getResponse.andExpect(status().isOk())
                    .andExpect(content().contentTypeCompatibleWith(JSON))
                    .andExpect(content().encoding(StandardCharsets.UTF_8))
                    .andExpect(content().json("{\"id\":\"" + id + "\",\"nodes\":null}"));
        }

        @Test
        void withoutMatchingId() throws Exception {
            String id = UUID.randomUUID().toString();

            given(executionEnvironmentService.findById(any(), any())).willReturn(null);
            given(executionEnvironmentService.findById(any(), eq(id))).willReturn(new ExecutionEnvironment().id(id));

            ResultActions getResponse = mockMvc.perform(get(getByIdUrl, "test").accept(JSON));
            getResponse.andExpect(status().isNotFound());
        }
    }

    @Nested
    class getAllNodes {

        public static final String getAllUrl = "/environments/{environmentId}/nodes";

        @Test
        void withoutMatchingEnv() throws Exception {
            String id = UUID.randomUUID().toString();

            given(executionEnvironmentService.findById(any(), eq(id))).willReturn(null);

            ResultActions getResponse = mockMvc.perform(get(getAllUrl, id).accept(JSON));
            getResponse.andExpect(status().isNotFound());
        }

        @Test
        void withoutFilter() throws Exception {
            String id = UUID.randomUUID().toString();
            List<NodeInformation> nodes = Collections.singletonList(new NodeInformation().id("test"));

            given(executionEnvironmentService.findById(any(), eq(id))).willReturn(new ExecutionEnvironment().id(id).nodes(nodes));
            given(nodeInformationService.filter(any(), any(), any())).willReturn(Collections.singletonList(new NodeInformation().id("test")).stream());

            ResultActions getResponse = mockMvc.perform(get(getAllUrl, id).accept(JSON));
            getResponse.andExpect(status().isOk())
                    .andExpect(content().contentTypeCompatibleWith(JSON))
                    .andExpect(content().encoding(StandardCharsets.UTF_8))
                    .andExpect(content().json("[{\"id\":\"test\",\"capacity\":null,\"allocatable\":null}]"));
        }

        @Test
        void passesAdditionalOptions() throws Exception {
            String id = UUID.randomUUID().toString();
            List<NodeInformation> nodes = Collections.singletonList(new NodeInformation().id("test"));
            QueryOptions options = new QueryOptions(0, Collections.singletonList("test"), Collections.singletonList("test2"), "cursor", Direction.After);

            given(executionEnvironmentService.findById(any(), eq(id))).willReturn(new ExecutionEnvironment().id(id).nodes(nodes));
            given(nodeInformationService.filter(any(), eq(options), any())).willReturn(Collections.singletonList(new NodeInformation().id("test")).stream());

            ResultActions getResponse = mockMvc.perform(get(getAllUrl, id).queryParam("size", "0")
                    .queryParam("sort", "test")
                    .queryParam("filter", "test2")
                    .queryParam("cursor", "cursor")
                    .queryParam("direction", "after")
                    .accept(JSON));
            getResponse.andExpect(status().isOk())
                    .andExpect(content().contentTypeCompatibleWith(JSON))
                    .andExpect(content().encoding(StandardCharsets.UTF_8))
                    .andExpect(content().json("[{\"id\":\"test\",\"capacity\":null,\"allocatable\":null}]"));
        }
    }

    @Nested
    class getNodeById {

        public static final String getByIdUrl = "/environments/{environmentId}/nodes/{nodeId}";


        @Test
        void withoutMatchingEnvironmentId() throws Exception {
            given(nodeInformationService.findById(any(), any(), any())).willReturn(null);

            ResultActions getResponse = mockMvc.perform(get(getByIdUrl, "test", "test").accept(JSON));
            getResponse.andExpect(status().isNotFound());
        }

        @Test
        void withMatchingId() throws Exception {
            String id = UUID.randomUUID().toString();
            String nodeId = UUID.randomUUID().toString();
            List<NodeInformation> nodes = Collections.singletonList(new NodeInformation().id(nodeId));

            given(executionEnvironmentService.findById(any(), eq(id))).willReturn(new ExecutionEnvironment().id(id).nodes(nodes));
            given(nodeInformationService.findById(any(), eq(nodeId), any())).willReturn(new NodeInformation().id(nodeId));

            ResultActions getResponse = mockMvc.perform(get(getByIdUrl, id, nodeId).accept(JSON));
            getResponse.andExpect(status().isOk())
                    .andExpect(content().contentTypeCompatibleWith(JSON))
                    .andExpect(content().encoding(StandardCharsets.UTF_8))
                    .andExpect(content().json("{\"id\":\"" + nodeId + "\",\"capacity\":null,\"allocatable\":null}"));
        }
    }
}
