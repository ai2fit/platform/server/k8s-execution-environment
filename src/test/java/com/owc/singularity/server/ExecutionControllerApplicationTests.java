package com.owc.singularity.server;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = ExecutionControllerApplication.class)
class ExecutionControllerApplicationTests {

    @Test
    void contextLoads() {}

}
