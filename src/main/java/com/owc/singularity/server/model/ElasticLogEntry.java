package com.owc.singularity.server.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ElasticLogEntry {

    @JsonProperty("@timestamp")
    public String timestamp;

    @JsonProperty("@timestamp")
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @JsonProperty("log")
    public String log;

    @JsonProperty("log")
    public void setLog(String log) {
        this.log = log;
    }
}
