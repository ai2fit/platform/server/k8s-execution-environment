package com.owc.singularity.server.model;

import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;

public class MountConfiguration {

    @JsonProperty("path")
    private String path;

    @JsonProperty("type")
    private String type;

    @JsonProperty("options")
    private Map<String, String> options;

    public MountConfiguration path(String path) {
        this.path = path;
        return this;
    }

    /**
     * Get path
     * 
     * @return path
     **/
    @Schema(description = "")

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public MountConfiguration type(String type) {
        this.type = type;
        return this;
    }

    /**
     * Get type
     * 
     * @return type
     **/
    @Schema(description = "")

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public MountConfiguration options(Map<String, String> options) {
        this.options = options;
        return this;
    }

    /**
     * Get options
     * 
     * @return options
     **/
    @Schema(description = "")

    public Map<String, String> getOptions() {
        return options;
    }

    public void setOptions(Map<String, String> options) {
        this.options = options;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MountConfiguration mountConfiguration = (MountConfiguration) o;
        return Objects.equals(this.path, mountConfiguration.path) && Objects.equals(this.type, mountConfiguration.type)
                && Objects.equals(this.options, mountConfiguration.options);
    }

    @Override
    public int hashCode() {
        return Objects.hash(path, type, options);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class JobConfiguration {\n");

        sb.append("    path: ").append(toIndentedString(path)).append("\n");
        sb.append("    type: ").append(toIndentedString(type)).append("\n");
        sb.append("    options: ").append(toIndentedString(options)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces (except the first
     * line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
