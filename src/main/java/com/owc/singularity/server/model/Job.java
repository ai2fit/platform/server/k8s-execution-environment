package com.owc.singularity.server.model;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.bson.types.ObjectId;
import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;

/**
 * Job
 */
@Validated
public class Job {

    @JsonProperty("_id")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId objectId = null;

    @JsonProperty("id")
    private String id = null;

    @JsonProperty("name")
    private String name = null;

    @JsonProperty("configuration")
    private JobConfiguration _configuration = null;

    @JsonProperty("userId")
    private String userId = null;

    @JsonProperty("description")
    private String description = null;

    @JsonProperty("submittedAt")
    private Instant submittedAt = null;

    @JsonProperty("executions")
    @Valid
    private List<Execution> executions = null;

    @JsonProperty("canceled")
    private boolean canceled = false;

    public Job id(String id) {
        this.id = id;
        return this;
    }

    /**
     * Get objectId
     * 
     * @return objectId
     **/
    @Schema(description = "")

    public ObjectId getObjectId() {
        return objectId;
    }

    public void setObjectId(ObjectId objectId) {
        this.objectId = objectId;
    }

    /**
     * Get id
     * 
     * @return id
     **/
    @Schema(description = "")

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Job name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Get name
     * 
     * @return name
     **/
    @Schema(description = "")

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Job _configuration(JobConfiguration _configuration) {
        this._configuration = _configuration;
        return this;
    }

    /**
     * Get _configuration
     * 
     * @return _configuration
     **/
    @Schema(description = "")

    @Valid
    public JobConfiguration getConfiguration() {
        return _configuration;
    }

    public void setConfiguration(JobConfiguration _configuration) {
        this._configuration = _configuration;
    }

    public Job userId(String userId) {
        this.userId = userId;
        return this;
    }

    /**
     * Get userId
     * 
     * @return userId
     **/
    @Schema(description = "")

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Job description(String description) {
        this.description = description;
        return this;
    }

    /**
     * Get description
     * 
     * @return description
     **/
    @Schema(description = "")

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Job submittedAt(Instant submittedAt) {
        this.submittedAt = submittedAt;
        return this;
    }

    /**
     * Get submittedAt
     * 
     * @return submittedAt
     **/
    @Schema(description = "")

    @Valid
    public Instant getSubmittedAt() {
        return submittedAt;
    }

    public void setSubmittedAt(Instant submittedAt) {
        this.submittedAt = submittedAt;
    }

    public Job executions(List<Execution> executions) {
        this.executions = executions;
        return this;
    }

    public Job addExecutionsItem(Execution executionsItem) {
        if (this.executions == null) {
            this.executions = new ArrayList<Execution>();
        }
        this.executions.add(executionsItem);
        return this;
    }

    /**
     * Get executions
     * 
     * @return executions
     **/
    @Schema(description = "")
    @Valid
    public List<Execution> getExecutions() {
        return executions;
    }

    public void setExecutions(List<Execution> executions) {
        this.executions = executions;
    }

    /**
     * Get canceled
     * 
     * @return canceled
     **/
    @Schema(description = "")

    public boolean getCanceled() {
        return canceled;
    }

    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Job job = (Job) o;
        return Objects.equals(this.id, job.id) && Objects.equals(this.name, job.name) && Objects.equals(this._configuration, job._configuration)
                && Objects.equals(this.userId, job.userId) && Objects.equals(this.description, job.description)
                && Objects.equals(this.submittedAt, job.submittedAt) && Objects.equals(this.executions, job.executions)
                && Objects.equals(this.canceled, job.canceled);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, _configuration, userId, description, submittedAt, executions, canceled);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Job {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    _configuration: ").append(toIndentedString(_configuration)).append("\n");
        sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
        sb.append("    description: ").append(toIndentedString(description)).append("\n");
        sb.append("    submittedAt: ").append(toIndentedString(submittedAt)).append("\n");
        sb.append("    executions: ").append(toIndentedString(executions)).append("\n");
        sb.append("    canceled: ").append(toIndentedString(canceled)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces (except the first
     * line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
