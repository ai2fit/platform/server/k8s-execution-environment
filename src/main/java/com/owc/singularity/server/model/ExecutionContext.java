package com.owc.singularity.server.model;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.owc.singularity.server.model.module.VersionNumber;

import io.swagger.v3.oas.annotations.media.Schema;

public class ExecutionContext {

    @JsonProperty("nodeId")
    private String nodeId;

    @JsonProperty("logLevel")
    private String logLevel;

    @JsonProperty("mounts")
    private List<MountConfiguration> mounts;

    @JsonProperty("modules")
    private Map<String, VersionNumber> modules;

    @JsonProperty("variables")
    private Map<String, String> variables;

    @JsonProperty("resources")
    private NodeResources resources;

    public ExecutionContext nodeId(String nodeId) {
        this.nodeId = nodeId;
        return this;
    }

    /**
     * Get nodeId
     * 
     * @return nodeId
     **/
    @Schema(description = "")

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public ExecutionContext logLevel(String logLevel) {
        this.logLevel = logLevel;
        return this;
    }

    /**
     * Get log level
     *
     * @return log level
     **/
    @Schema(description = "")
    public String getLogLevel() {
        return logLevel;
    }

    public void setLogLevel(String logLevel) {
        this.logLevel = logLevel;
    }

    public ExecutionContext mounts(List<MountConfiguration> mounts) {
        this.mounts = mounts;
        return this;
    }

    /**
     * Get mounts
     * 
     * @return mounts
     **/
    @Schema(description = "")

    public List<MountConfiguration> getMounts() {
        return mounts;
    }

    public void setMounts(List<MountConfiguration> mounts) {
        this.mounts = mounts;
    }

    public ExecutionContext modules(Map<String, VersionNumber> modules) {
        this.modules = modules;
        return this;
    }

    /**
     * Get modules
     * 
     * @return modules
     **/
    @Schema(description = "")

    public Map<String, VersionNumber> getModules() {
        return modules;
    }

    public void setModules(Map<String, VersionNumber> modules) {
        this.modules = modules;
    }

    public ExecutionContext variables(Map<String, String> variables) {
        this.variables = variables;
        return this;
    }

    /**
     * Get variables
     *
     * @return variables
     **/
    @Schema(description = "")
    public Map<String, String> getVariables() {
        return variables;
    }

    public void setVariables(Map<String, String> variables) {
        this.variables = variables;
    }

    public ExecutionContext resources(NodeResources resources) {
        this.resources = resources;
        return this;
    }

    /**
     * Get resources
     * 
     * @return resources
     **/
    @Schema(description = "")

    public NodeResources getResources() {
        return resources;
    }

    public void setResources(NodeResources resources) {
        this.resources = resources;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ExecutionContext context = (ExecutionContext) o;
        return Objects.equals(this.nodeId, context.nodeId) && Objects.equals(this.mounts, context.mounts) && Objects.equals(this.resources, context.resources);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nodeId, mounts, resources);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class JobConfiguration {\n");

        sb.append("    nodeId: ").append(toIndentedString(nodeId)).append("\n");
        sb.append("    mounts: ").append(toIndentedString(mounts)).append("\n");
        sb.append("    resources: ").append(toIndentedString(resources)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces (except the first
     * line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
