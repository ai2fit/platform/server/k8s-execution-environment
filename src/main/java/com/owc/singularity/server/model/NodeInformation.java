package com.owc.singularity.server.model;

import java.util.Objects;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;

/**
 * NodeInformation
 */
@Validated
public class NodeInformation {

    @JsonProperty("id")
    private String id = null;

    @JsonProperty("allocatable")
    private NodeResources allocatable = null;

    @JsonProperty("capacity")
    private NodeResources capacity = null;

    public NodeInformation id(String id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     * 
     * @return id
     **/
    @Schema(description = "")

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public NodeInformation allocatable(NodeResources allocatable) {
        this.allocatable = allocatable;
        return this;
    }

    /**
     * Get allocatable
     * 
     * @return allocatable
     **/
    @Schema(description = "")

    @Valid
    public NodeResources getAllocatable() {
        return allocatable;
    }

    public void setAllocatable(NodeResources allocatable) {
        this.allocatable = allocatable;
    }

    public NodeInformation capacity(NodeResources capacity) {
        this.capacity = capacity;
        return this;
    }

    /**
     * Get capacity
     * 
     * @return capacity
     **/
    @Schema(description = "")

    @Valid
    public NodeResources getCapacity() {
        return capacity;
    }

    public void setCapacity(NodeResources capacity) {
        this.capacity = capacity;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NodeInformation nodeInformation = (NodeInformation) o;
        return Objects.equals(this.id, nodeInformation.id) && Objects.equals(this.allocatable, nodeInformation.allocatable)
                && Objects.equals(this.capacity, nodeInformation.capacity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, allocatable, capacity);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class NodeInformation {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    allocatable: ").append(toIndentedString(allocatable)).append("\n");
        sb.append("    capacity: ").append(toIndentedString(capacity)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces (except the first
     * line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
