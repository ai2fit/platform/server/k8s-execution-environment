package com.owc.singularity.server.model.module;

import java.time.Instant;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.owc.singularity.server.api.JobsApi;

import io.swagger.v3.oas.annotations.media.Schema;

public class ModuleInfo {

    /** The name of the module. */
    @JsonProperty("_id")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId objectId = null;

    /**
     * Get objectId
     * 
     * @return objectId
     **/
    @Schema(description = "")

    public ObjectId getObjectId() {
        return objectId;
    }

    public void setObjectId(ObjectId objectId) {
        this.objectId = objectId;
    }

    /** The name of the plugin. */
    @JsonProperty("name")
    private String name;

    /**
     * Get name
     * 
     * @return name
     **/
    @Schema(description = "")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /** The version of the plugin. */
    @JsonProperty("version")
    private VersionNumber version;

    /**
     * Get version
     * 
     * @return version
     **/
    @Schema(description = "")
    public VersionNumber getVersion() {
        return version;
    }

    public void setVersion(VersionNumber version) {
        this.version = version;
    }

    /** The vendor of the plugin. */
    @JsonProperty("vendor")
    private String vendor;

    /**
     * Get vendor
     * 
     * @return vendor
     **/
    @Schema(description = "")
    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    /** The SingularityEngine version which is needed for this plugin. */
    @JsonProperty("requiredEngineVersion")
    private VersionNumber requiredEngineVersion;

    /**
     * Get requiredEngineVersion
     * 
     * @return requiredEngineVersion
     **/
    @Schema(description = "")
    public VersionNumber getRequiredEngineVersion() {
        return requiredEngineVersion;
    }

    public void setRequiredEngineVersion(VersionNumber requiredEngineVersion) {
        this.requiredEngineVersion = requiredEngineVersion;
    }

    /** The moduleKey of the plugin. */
    @JsonProperty("moduleKey")
    private String moduleKey;

    /**
     * Get moduleKey
     * 
     * @return moduleKey
     **/
    @Schema(description = "")
    public String getModuleKey() {
        return moduleKey;
    }

    public void setModuleKey(String moduleKey) {
        this.moduleKey = moduleKey;
    }

    /** The moduleDependencies of the plugin. */
    @JsonProperty("moduleDependencies")
    private List<ModuleDependency> moduleDependencies;

    /**
     * Get moduleDependencies
     * 
     * @return moduleDependencies
     **/
    @Schema(description = "")
    public List<ModuleDependency> getModuleDependencies() {
        return moduleDependencies;
    }

    public void setModuleDependencies(List<ModuleDependency> moduleDependencies) {
        this.moduleDependencies = moduleDependencies;
    }

    /** The relative filePath of the plugin. */
    @JsonProperty("filePath")
    private String filePath = null;

    /**
     * Get filePath
     * 
     * @return filePath
     **/
    @Schema(description = "")
    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /** Time of the plugin upload. */
    @JsonProperty("uploadedAt")
    private Instant uploadedAt = null;

    /**
     * Get uploadedAt
     * 
     * @return uploadedAt
     **/
    @Schema(description = "")
    public Instant getUploadedAt() {
        return uploadedAt;
    }

    public void setUploadedAt(Instant uploadedAt) {
        this.uploadedAt = uploadedAt;
    }

    /**
     * Create an incomplete ModuleInfo for comparison
     * 
     * @param entry
     *            entry with key=moduleKey and value=version
     * @return An incomplete module info definition to map from a {@link JobsApi.JobSubmission} to a
     *         fully defined ModuleInfo
     */
    public static ModuleInfo of(Entry<String, VersionNumber> entry) {
        ModuleInfo info = new ModuleInfo();
        info.setModuleKey(entry.getKey());
        info.setVersion(entry.getValue());

        return info;
    }

    public String toFileName() {
        String fileName = this.vendor + "-" + this.moduleKey + "-" + this.name + "-" + this.version + ".jar";
        return fileName.replaceAll("[^a-zA-Z0-9\\._]+", "_");
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ModuleInfo module = (ModuleInfo) o;
        return Objects.equals(this.version, module.version) && Objects.equals(this.moduleKey, module.moduleKey);
    }

    @Override
    public int hashCode() {
        return Objects.hash(version, moduleKey);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ModuleInfo {\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    version: ").append(toIndentedString(version)).append("\n");
        sb.append("    vendor: ").append(toIndentedString(vendor)).append("\n");
        sb.append("    requiredEngineVersion: ").append(toIndentedString(requiredEngineVersion)).append("\n");
        sb.append("    moduleKey: ").append(toIndentedString(moduleKey)).append("\n");
        sb.append("    moduleDependencies: ").append(toIndentedString(moduleDependencies)).append("\n");
        sb.append("    filePath: ").append(toIndentedString(filePath)).append("\n");
        sb.append("    uploadedAt: ").append(toIndentedString(uploadedAt)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces (except the first
     * line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
