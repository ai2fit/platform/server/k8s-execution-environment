package com.owc.singularity.server.model.worker;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WorkerMountConfiguration {

    @JsonProperty("mountType")
    final String type;

    @JsonProperty("options")
    final Map<String, String> options;

    public WorkerMountConfiguration(String type, Map<String, String> options) {
        this.type = type;
        this.options = options;
    }
}
