package com.owc.singularity.server.model;

import java.util.Objects;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * JobConfiguration
 */
@Validated
public class JobConfiguration {

    @JsonProperty("context")
    private ExecutionContext context = null;

    @JsonProperty("environmentId")
    private String environmentId = null;

    @JsonProperty("pipelinePath")
    @JsonAlias("processPath")
    private String pipelinePath = null;

    public JobConfiguration context(ExecutionContext context) {
        this.context = context;
        return this;
    }

    /**
     * Get context
     * 
     * @return context
     **/
    @Schema(description = "")

    public ExecutionContext getContext() {
        return context;
    }

    public void setContext(ExecutionContext context) {
        this.context = context;
    }

    public JobConfiguration pipelinePath(String processPath) {
        this.pipelinePath = processPath;
        return this;
    }

    /**
     * Get processPath
     * 
     * @return processPath
     **/
    @Schema(description = "")
    public String getPipelinePath() {
        return pipelinePath;
    }

    public void setPipelinePath(String pipelinePath) {
        this.pipelinePath = pipelinePath;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        JobConfiguration jobConfiguration = (JobConfiguration) o;
        return Objects.equals(this.context, jobConfiguration.context) && Objects.equals(this.pipelinePath, jobConfiguration.pipelinePath);
    }

    @Override
    public int hashCode() {
        return Objects.hash(context, pipelinePath);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class JobConfiguration {\n");

        sb.append("    context: ").append(toIndentedString(context)).append("\n");
        sb.append("    pipelinePath: ").append(toIndentedString(pipelinePath)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces (except the first
     * line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
