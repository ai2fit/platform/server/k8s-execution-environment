package com.owc.singularity.server.model;

import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;

public class ExecutionEnvironment {

    @JsonProperty("id")
    private String id;

    @JsonProperty("nodes")
    private List<NodeInformation> nodes;

    public ExecutionEnvironment id(String id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     * 
     * @return id
     */
    @Schema(description = "")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ExecutionEnvironment nodes(List<NodeInformation> nodes) {
        this.nodes = nodes;
        return this;
    }

    /**
     * Get nodes
     * 
     * @return nodes
     */
    @Schema(description = "")
    public List<NodeInformation> getNodes() {
        return nodes;
    }

    public void setNodes(List<NodeInformation> nodes) {
        this.nodes = nodes;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ExecutionEnvironment environment = (ExecutionEnvironment) o;
        return Objects.equals(this.id, environment.id) && Objects.equals(this.nodes, environment.nodes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nodes);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class JobConfiguration {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    nodes: ").append(toIndentedString(nodes)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces (except the first
     * line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
