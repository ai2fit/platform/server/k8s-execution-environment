package com.owc.singularity.server.model.module;


import java.util.Locale;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;


/**
 * Contains information about the different parts of a version number.
 *
 * @author Ingo Mierswa, Marcel Michel
 */
public class VersionNumber implements Comparable<VersionNumber> {


    public final static VersionNumber Undefined = new VersionNumber(0, 0, 0) {

        @Override
        public boolean equals(Object obj) {
            return this == obj;
        }

        @Override
        public String toString() {
            return "Undefined";
        }
    };

    /**
     * Exception for malformed VersionNumber String representations including further information
     * 
     * @since 8.0
     */
    public static class VersionNumberException extends RuntimeException {

        public VersionNumberException(String message) {
            super(message);
        }
    }

    private static final String CLASSIFIER_TAG = "-";

    private static final String SNAPSHOT_TAG = "snapshot";
    private static final String WORK_IN_PROGRESS_TAG = "wip";

    private final int majorNumber;

    private final int minorNumber;

    private final int patchLevel;

    private final String classifier;

    /**
     * Constructs a new VersionNumber object with the given versionString. The versionString should
     * use the format major.minor.patch-classifier. Examples: 6.0 or 6.0.005-SNAPSHOT. Throws a
     * {@link VersionNumberException} if the given versionString is malformed.
     */
    @JsonCreator
    public VersionNumber(String versionString) {
        String version = versionString.trim();
        String[] tokens = version.split("\\.", 4);

        // extract major, minor and patch level version
        if (tokens.length > 0) {
            try {
                majorNumber = Integer.parseInt(tokens[0]);
            } catch (NumberFormatException e) {
                throw new VersionNumberException("Malformed major version!");
            }
        } else {
            throw new VersionNumberException("No major version given!");
        }
        if (tokens.length > 1) {
            try {
                minorNumber = Integer.parseInt(tokens[1]);
            } catch (NumberFormatException e) {
                throw new VersionNumberException("Malformed minor version!");
            }
        } else {
            minorNumber = 0;
        }
        if (tokens.length > 2) {
            String patchNumber;
            int classifierIndex = tokens[2].indexOf(CLASSIFIER_TAG);
            if (classifierIndex > 0 && classifierIndex < tokens[2].length() - 1) {
                classifier = tokens[2].substring(classifierIndex + 1);
                patchNumber = tokens[2].substring(0, classifierIndex);
            } else {
                classifier = null;
                patchNumber = tokens[2];
            }
            try {
                patchLevel = Integer.parseInt(patchNumber);
            } catch (NumberFormatException e) {
                throw new VersionNumberException("Malformed patch level!");
            }
        } else {
            patchLevel = 0;
            classifier = null;
        }
    }

    /**
     * Constructs a new VersionNumber object with the given major, minor version and patch level.
     */
    public VersionNumber(int majorNumber, int minorNumber, int patchLevel) {
        this(majorNumber, minorNumber, patchLevel, null);
    }

    /**
     * Constructs a new VersionNumber object with the given major, minor version, patch level and
     * classifier. Note: A {@link #CLASSIFIER_TAG} will be added as prefix to the given classifier.
     */
    public VersionNumber(int majorNumber, int minorNumber, int patchLevel, String classifier) {
        this.majorNumber = majorNumber;
        this.minorNumber = minorNumber;
        this.patchLevel = patchLevel;
        this.classifier = classifier != null ? classifier.isBlank() ? null : classifier : null;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (classifier == null ? 0 : classifier.toLowerCase(Locale.ROOT).hashCode());
        result = prime * result + majorNumber;
        result = prime * result + minorNumber;
        result = prime * result + patchLevel;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        VersionNumber other = (VersionNumber) obj;

        if (majorNumber != other.majorNumber) {
            return false;
        }
        if (minorNumber != other.minorNumber) {
            return false;
        }
        if (patchLevel != other.patchLevel) {
            return false;
        }
        return this.compareClassifier(other) == 0;
    }

    /**
     * Returns if this number is at least as high as the given version number object.
     */
    public boolean isAtLeast(VersionNumber other) {
        return this.compareTo(other) >= 0;
    }

    /**
     * Returns <code>true</code> if this VersionNumber is at most as high as the given argument.
     */
    public boolean isAtMost(VersionNumber other) {
        return this.compareTo(other) <= 0;
    }

    /**
     * Returns <code>true</code> if this VersionNumber is above (greater than) the given argument.
     */
    public boolean isAbove(VersionNumber other) {
        return !isAtMost(other);
    }

    @Override
    public int compareTo(VersionNumber other) {
        if (this == other)
            return 0;
        if (other == null) {
            return 1;
        }
        int index = Integer.compare(this.majorNumber, other.majorNumber);
        if (index != 0) {
            return index;
        } else {
            index = Integer.compare(this.minorNumber, other.minorNumber);
            if (index != 0) {
                return index;
            } else {
                index = Integer.compare(this.patchLevel, other.patchLevel);
                if (index != 0) {
                    return index;
                }
                return compareClassifier(other);
            }
        }
    }

    private int compareClassifier(VersionNumber other) {
        if (Objects.equals(classifier, other.classifier))
            return 0;
        if (classifier != null && other.classifier != null) {
            if (isDevelopmentBuild() && other.isDevelopmentBuild()) {
                if (classifier.length() == other.classifier.length()) {
                    // possibly the same pattern
                    String[] tokens = classifier.split("-");
                    String[] otherTokens = other.classifier.split("-");
                    if (tokens.length == otherTokens.length) {
                        for (int i = 0; i < tokens.length; i++) {
                            try {
                                int parsed = Integer.parseInt(tokens[i]);
                                int otherParsed = Integer.parseInt(otherTokens[i]);
                                int compare = Integer.compare(parsed, otherParsed);
                                if (compare != 0)
                                    return compare;
                            } catch (Exception e) {
                                break;
                            }
                        }
                    }
                }
                return classifier.compareTo(other.classifier);
            }
            if (isDevelopmentBuild()) {
                return 1;
            } else {
                return -1;
            }
        } else {
            if (classifier != null)
                return 1;
            else {
                return -1;
            }
        }
    }

    @Override
    @JsonValue
    public String toString() {
        return majorNumber + "." + minorNumber + "." + "000".substring((patchLevel + "").length()) + patchLevel + (classifier != null ? "-" + classifier : "");
    }

    /**
     * Assembles the String representation to be a minimal representation without adding leading
     * zeros to the patchlevel
     * 
     * @return a minimal String representation of this VersionNumber
     */
    public String getShortLongVersion() {
        return majorNumber + "." + minorNumber + "." + patchLevel + (classifier != null ? classifier.toUpperCase(Locale.ENGLISH) : "");
    }

    /**
     * Returns the SingularityEngine version in the format major.minor.patchlevel-classifier, with 3
     * digits for patchlevel. Example: 6.0.005-SNAPSHOT
     */
    public String getLongVersion() {
        return toString();
    }

    /**
     * Return the SingularityEngine short version in the format major.minor. Example: 6.0
     */
    public String getShortVersion() {
        return majorNumber + "." + minorNumber;
    }

    /**
     * Return the SingularityEngine major version.
     */
    public int getMajorNumber() {
        return majorNumber;
    }

    /**
     * Return the SingularityEngine minor version.
     */
    public int getMinorNumber() {
        return minorNumber;
    }

    /**
     * Return the SingularityEngine path level.
     */
    public int getPatchLevel() {
        return patchLevel;
    }

    /**
     * @return <code>true</code> if the current version is a development build (i.e. it has a
     *         classifier named SNAPSHOT or ALPHA or BETA or RC).
     */
    public final boolean isDevelopmentBuild() {
        return isTaggedWith(SNAPSHOT_TAG) || isTaggedWith(WORK_IN_PROGRESS_TAG);
    }

    private boolean isTaggedWith(String tagName) {
        if (classifier != null) {
            return classifier.toLowerCase(Locale.ROOT).endsWith(tagName);
        }
        return false;
    }
}
