package com.owc.singularity.server.model;

import java.time.Instant;
import java.util.Objects;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;


/**
 * Execution
 */
@Validated
public class Execution {

    @JsonProperty("id")
    private String id = null;

    @JsonProperty("submittedAt")
    private Instant submittedAt = null;

    @JsonProperty("startedAt")
    private Instant startedAt = null;

    @JsonProperty("stoppedAt")
    private Instant stoppedAt = null;

    /**
     * Gets or Sets status
     */
    public enum StatusEnum {

        PENDING("pending"),

        RUNNING("running"),

        SUCCESS("success"),

        FAIL("fail"),

        STOPPED("stopped"),

        UNKNOWN("unknown");

        private final String value;

        StatusEnum(String value) {
            this.value = value;
        }

        @Override
        @JsonValue
        public String toString() {
            return String.valueOf(value);
        }

        @JsonCreator
        public static StatusEnum fromValue(String text) {
            for (StatusEnum b : StatusEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            return null;
        }
    }

    @JsonProperty("status")
    private StatusEnum status = null;

    public Execution id(String id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     * 
     * @return id
     **/
    @Schema(description = "")

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Execution submittedAt(Instant submittedAt) {
        this.submittedAt = submittedAt;
        return this;
    }

    /**
     * Get submittedAt
     * 
     * @return submittedAt
     **/
    @Schema(description = "")

    @Valid
    public Instant getSubmittedAt() {
        return submittedAt;
    }

    public void setSubmittedAt(Instant submittedAt) {
        this.submittedAt = submittedAt;
    }

    public Execution startedAt(Instant startedAt) {
        this.startedAt = startedAt;
        return this;
    }

    /**
     * Get startedAt
     * 
     * @return startedAt
     **/
    @Schema(description = "")

    @Valid
    public Instant getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(Instant startedAt) {
        this.startedAt = startedAt;
    }

    public Execution stoppedAt(Instant stoppedAt) {
        this.stoppedAt = stoppedAt;
        return this;
    }

    /**
     * Get stopedAt
     * 
     * @return stopedAt
     **/
    @Schema(description = "")

    @Valid
    public Instant getStoppedAt() {
        return stoppedAt;
    }

    public void setStoppedAt(Instant stoppedAt) {
        this.stoppedAt = stoppedAt;
    }

    public Execution status(StatusEnum status) {
        this.status = status;
        return this;
    }

    /**
     * Get status
     * 
     * @return status
     **/
    @Schema(description = "")

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Execution execution = (Execution) o;
        return Objects.equals(this.id, execution.id) && Objects.equals(this.submittedAt, execution.submittedAt)
                && Objects.equals(this.startedAt, execution.startedAt) && Objects.equals(this.stoppedAt, execution.stoppedAt)
                && Objects.equals(this.status, execution.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, submittedAt, startedAt, stoppedAt, status);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Execution {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    submittedAt: ").append(toIndentedString(submittedAt)).append("\n");
        sb.append("    startedAt: ").append(toIndentedString(startedAt)).append("\n");
        sb.append("    stoppedAt: ").append(toIndentedString(stoppedAt)).append("\n");
        sb.append("    status: ").append(toIndentedString(status)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces (except the first
     * line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
