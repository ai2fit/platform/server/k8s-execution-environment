package com.owc.singularity.server.model.module;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.jar.Attributes;
import java.util.jar.JarInputStream;
import java.util.jar.Manifest;

import com.owc.singularity.server.model.module.VersionNumber.VersionNumberException;

public class ModuleManifest {

    private static final String MANIFEST_ENTRY_SINGULARITY_VERSION = "Singularity-Version";
    private static final String MANIFEST_ENTRY_MODULE_KEY = "Module-Key";
    private static final String MANIFEST_ENTRY_MODULE_NAME = "Module-Name";
    private static final String MANIFEST_ENTRY_MODULE_DEPENDENCIES = "Module-Dependencies";
    private static final String MANIFEST_ENTRY_MODULE_VENDOR = "Implementation-Vendor";
    private static final String MANIFEST_ENTRY_MODULE_VERSION = "Implementation-Version";


    private ModuleManifest() {

    }

    public static ModuleInfo extractModuleInfo(JarInputStream jarInputStream) throws IOException {
        ModuleInfo moduleInfo = new ModuleInfo();

        Manifest manifest = jarInputStream.getManifest();
        if (manifest == null)
            throw new IllegalArgumentException("Jar file does not provide a MANIFEST entry");
        Attributes manifestAttributes = manifest.getMainAttributes();
        // required attributes
        moduleInfo.setName(getManifestValue(manifestAttributes, MANIFEST_ENTRY_MODULE_NAME));
        moduleInfo.setVersion(getManifestValueAsVersion(manifestAttributes, MANIFEST_ENTRY_MODULE_VERSION, VersionNumber.Undefined));
        moduleInfo.setModuleKey(getManifestValue(manifestAttributes, MANIFEST_ENTRY_MODULE_KEY));
        moduleInfo.setRequiredEngineVersion(getManifestValueAsVersion(manifestAttributes, MANIFEST_ENTRY_SINGULARITY_VERSION, new VersionNumber(0, 0, 0)));

        // optional values
        String moduleDependencyString = getManifestValue(manifestAttributes, MANIFEST_ENTRY_MODULE_DEPENDENCIES);
        if (moduleDependencyString != null) {
            List<ModuleDependency> manifestModuleDependencies;
            try {
                manifestModuleDependencies = ModuleDependency.allOf(moduleDependencyString);
            } catch (IllegalArgumentException | VersionNumberException e) {
                manifestModuleDependencies = null;
            }
            moduleInfo.setModuleDependencies(manifestModuleDependencies);
        } else {
            moduleInfo.setModuleDependencies(Collections.emptyList());
        }
        moduleInfo.setVendor(getManifestValue(manifestAttributes, MANIFEST_ENTRY_MODULE_VENDOR));

        return moduleInfo;
    }

    private static VersionNumber getManifestValueAsVersion(Attributes manifestAttributes, String manifestEntry, VersionNumber defaultVersion) {
        try {
            String manifestValue = getManifestValue(manifestAttributes, manifestEntry);
            if (manifestValue == null) {
                return defaultVersion;
            }
            return new VersionNumber(manifestValue);
        } catch (VersionNumberException e) {
            return null;
        }
    }

    /**
     * This method returns true if the file is having a complete module manifest.
     * 
     * @return true if the file is a module from the description
     */
    public static boolean isModule(ModuleInfo moduleInfo) {
        String name = moduleInfo.getName();
        VersionNumber version = moduleInfo.getVersion();
        String moduleKey = moduleInfo.getModuleKey();
        VersionNumber requiredEngineVersion = moduleInfo.getRequiredEngineVersion();
        List<ModuleDependency> moduleDependencies = moduleInfo.getModuleDependencies();

        return name != null && !name.isEmpty() && version != null && moduleKey != null && !moduleKey.isEmpty() && requiredEngineVersion != null
                && moduleDependencies != null;
    }

    private static String getManifestValue(Attributes attributes, String key) {
        String result = attributes.getValue(key);
        if (result != null) {
            result = result.trim();
            if (result.isEmpty())
                result = null;
        }
        return result;
    }
}
