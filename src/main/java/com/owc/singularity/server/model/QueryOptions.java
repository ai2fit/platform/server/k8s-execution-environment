package com.owc.singularity.server.model;

import java.util.*;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.owc.singularity.server.configuration.format.CaseInsensitiveEnum;

import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Min;

public class QueryOptions {

    /**
     * The Page Size.
     */
    @Min(1)
    @Parameter(description = "The size of the page to be returned", schema = @Schema(type = "integer", defaultValue = "20", requiredMode = Schema.RequiredMode.REQUIRED))
    private int size;

    /**
     * The Sort.
     */
    @Parameter(description = "Sorting criteria in the format: property,(asc|desc). " + "Default sort order is ascending. "
            + "Multiple sort criteria are supported.", name = "sort", array = @ArraySchema(schema = @Schema(type = "string")))
    private List<String> sort;

    /**
     * The Cursor.
     */
    @Parameter(description = "The cursor of the query. A cursor defines an anchor for the pagination", schema = @Schema(type = "string"))
    private String cursor;

    @CaseInsensitiveEnum
    public enum Direction {

        Before("before"), After("after");

        private final String value;

        Direction(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return value;
        }

        @JsonCreator
        public static Direction fromValue(String text) {
            for (Direction b : Direction.values()) {
                if (String.valueOf(b.value).equalsIgnoreCase(text)) {
                    return b;
                }
            }
            return null;
        }
    }

    @Parameter(description = "The direction of pagination, i.e. whether to get the page before `cursor` or after it.", schema = @Schema(type = "string", allowableValues = {
            "before", "after" }))
    private Direction direction;

    /**
     * The filters.
     */
    @Parameter(description = "The query filters."
            + "Filters are a simple key=value map that contains query filters like (filter=user:'someone', filter=submittedAt:1659376563).", name = "filter", array = @ArraySchema(schema = @Schema(type = "string", pattern = "[a-zA-Z]*:.*")))
    private List<String> filter;


    public QueryOptions() {
        this.size = 20;
        this.sort = new ArrayList<>();
        this.filter = new ArrayList<>();
    }

    /**
     * Instantiates a new QueryOptions.
     *
     * @param size
     *            the size
     * @param sort
     *            the sort
     * @param filter
     *            the additional filters
     */
    public QueryOptions(int size, List<String> sort, List<String> filter) {
        this(size, sort, filter, null, null);
    }

    /**
     * Instantiates a consecutive QueryOptions.
     *
     * @param size
     *            the size
     * @param sort
     *            the sort
     * @param filter
     *            the additional filters
     * @param cursor
     *            the cursor
     * @param direction
     *            the query direction
     */
    public QueryOptions(int size, List<String> sort, List<String> filter, String cursor, Direction direction) {
        this.size = size;
        this.sort = Objects.requireNonNullElseGet(sort, ArrayList::new);
        this.filter = Objects.requireNonNullElseGet(filter, ArrayList::new);
        this.cursor = cursor;
        this.direction = direction;
    }

    /**
     * Gets size.
     *
     * @return the size
     */
    public Integer getSize() {
        return size;
    }

    /**
     * Sets size.
     *
     * @param size
     *            the size
     */
    public void setSize(Integer size) {
        this.size = size;
    }

    /**
     * Gets sort.
     *
     * @return the sort
     */
    public List<String> getSort() {
        return sort;
    }

    /**
     * Sets sort.
     *
     * @param sort
     *            the sort
     */
    public void setSort(List<String> sort) {
        if (sort == null) {
            this.sort.clear();
        } else {
            this.sort = sort;
        }
    }

    /**
     * Add sort.
     *
     * @param sort
     *            the sort
     */
    public void addSort(String sort) {
        this.sort.add(sort);
    }

    public String getCursor() {
        return cursor;
    }

    public void setCursor(String cursor) {
        this.cursor = cursor;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public List<String> getFilter() {
        return filter;
    }

    public void setFilter(List<String> filter) {
        if (filter == null) {
            this.filter.clear();
        } else {
            this.filter = filter;
        }
    }

    public void addFilter(String key, String value) {
        this.filter.add(key + ":" + value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        QueryOptions other = (QueryOptions) o;
        return Objects.equals(size, other.size) && Objects.equals(cursor, other.cursor) && Objects.equals(direction, other.direction)
                && Objects.equals(sort, other.sort) && Objects.equals(filter, other.filter);
    }

    @Override
    public int hashCode() {
        return Objects.hash(size, sort, cursor, direction, filter);
    }

    @Override
    public String toString() {
        return "QueryOptions{" + "size=" + size + ", sort=" + sort + ", cursor='" + cursor + '\'' + ", direction=" + direction + ", filter=" + filter + '}';
    }
}
