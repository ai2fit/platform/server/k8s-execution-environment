package com.owc.singularity.server.model.worker;

import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WorkerMounts {

    @JsonProperty("mounts")
    final Map<String, WorkerMountConfiguration> mountMap = new LinkedHashMap<>();

    public void add(String path, WorkerMountConfiguration configuration) {
        mountMap.put(path, configuration);
    }
}
