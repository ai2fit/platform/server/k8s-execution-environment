package com.owc.singularity.server.model.module;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ModuleDependency {

    @JsonProperty
    String moduleKey;

    @JsonProperty
    VersionNumber dependencyVersion;

    @JsonCreator
    public ModuleDependency(@JsonProperty("moduleKey") String moduleKey, @JsonProperty("dependencyVersion") VersionNumber dependencyVersion) {
        this.moduleKey = moduleKey;
        this.dependencyVersion = dependencyVersion;
    }

    public static List<ModuleDependency> allOf(String dependencies) {
        String[] dependencyStrings = dependencies.split(",");
        return Arrays.stream(dependencyStrings).map(ModuleDependency::of).collect(Collectors.toList());
    }

    public static ModuleDependency of(String dependency) {
        dependency = dependency.trim();
        int startVersion = dependency.indexOf("[");
        int endVersion = dependency.indexOf("]");
        if (startVersion == -1 || endVersion == -1 || endVersion < startVersion)
            throw new IllegalArgumentException("Dependency not valid: " + dependency);
        String dependentModuleKey = dependency.substring(0, startVersion);
        String dependentModuleVersion = dependency.substring(startVersion + 1, endVersion);
        return new ModuleDependency(dependentModuleKey, new VersionNumber(dependentModuleVersion));
    }
}
