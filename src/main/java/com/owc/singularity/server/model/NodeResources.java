package com.owc.singularity.server.model;

import java.util.Objects;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * NodeResources
 */
@Validated
public class NodeResources {

    @JsonProperty("cpu")
    private String cpu = null;

    @JsonProperty("memory")
    private String memory = null;

    @JsonProperty("storage")
    private String storage = null;

    public NodeResources cpu(String cpu) {
        this.cpu = cpu;
        return this;
    }

    /**
     * Get cpu
     * 
     * @return cpu
     **/
    @Schema(description = "")

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public NodeResources memory(String memory) {
        this.memory = memory;
        return this;
    }

    /**
     * Get memory
     * 
     * @return memory
     **/
    @Schema(description = "")

    public String getMemory() {
        return memory;
    }

    public void setMemory(String memory) {
        this.memory = memory;
    }

    public NodeResources storage(String storage) {
        this.storage = storage;
        return this;
    }

    /**
     * Get storage
     * 
     * @return storage
     **/
    @Schema(description = "")

    public String getStorage() {
        return storage;
    }

    public void setStorage(String storage) {
        this.storage = storage;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NodeResources nodeResources = (NodeResources) o;
        return Objects.equals(this.cpu, nodeResources.cpu) && Objects.equals(this.memory, nodeResources.memory)
                && Objects.equals(this.storage, nodeResources.storage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cpu, memory, storage);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class NodeResources {\n");

        sb.append("    cpu: ").append(toIndentedString(cpu)).append("\n");
        sb.append("    memory: ").append(toIndentedString(memory)).append("\n");
        sb.append("    storage: ").append(toIndentedString(storage)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces (except the first
     * line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
