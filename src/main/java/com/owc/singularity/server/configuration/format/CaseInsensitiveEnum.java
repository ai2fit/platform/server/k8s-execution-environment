package com.owc.singularity.server.configuration.format;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks an enum as case-insensitive, which will create a custom
 * {@link org.springframework.core.convert.converter.Converter Converter} of type
 * {@link CaseInsensitiveEnumConverter} that will compare enum names ignoring case. Such enums are
 * typically used in {@link org.springframework.web.bind.annotation.RequestParam @RequestParam} or
 * {@link org.springframework.web.bind.annotation.PathVariable @PathVariable}.
 *
 * @author Hatem Hamad
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface CaseInsensitiveEnum {

}
