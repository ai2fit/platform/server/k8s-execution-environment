package com.owc.singularity.server.configuration.format;


import org.springframework.core.convert.converter.Converter;


public class CaseInsensitiveEnumConverter<E extends Enum<?>> implements Converter<String, E> {

    private final Class<E> enumType;

    public CaseInsensitiveEnumConverter(Class<E> enumType) {
        if (enumType == null)
            throw new NullPointerException("enumType must not be null");
        if (!enumType.isEnum())
            throw new IllegalArgumentException(enumType.getName() + " is not an enum type");
        this.enumType = enumType;
    }

    @Override
    public E convert(String source) {
        for (final E each : enumType.getEnumConstants()) {
            if (each.name().equalsIgnoreCase(source)) {
                return each;
            }
        }
        return null;
    }
}
