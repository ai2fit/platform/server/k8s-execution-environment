package com.owc.singularity.server.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClientBuilder;

@Configuration
public class KubernetesClientConfiguration {

    @Bean
    public KubernetesClient getKubernetesClient() {
        return new KubernetesClientBuilder().build();
    }
}
