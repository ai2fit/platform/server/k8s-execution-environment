package com.owc.singularity.server.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;


@Configuration
public class OpenApiDocumentationConfig {

    @Bean
    public OpenAPI openApi() {
        return new OpenAPI().info(apiInfo());
    }

    Info apiInfo() {
        final String version;
        final Package thisPackage = OpenApiDocumentationConfig.class.getPackage();
        if (thisPackage != null && thisPackage.getImplementationVersion() != null) {
            version = thisPackage.getImplementationVersion();
        } else {
            version = "dev-build";
        }
        return new Info().title("Execution Server API")
                .description("This is a documentation of the Execution Server API")
                .license(new License().name("Apache 2.0").url("http://www.apache.org/licenses/LICENSE-2.0.html"))
                .termsOfService("https://oldworldcomputing.com/products/singularity/terms")
                .version(version)
                .contact(new Contact().name("Old World Computing GmbH").url("https://oldworldcomputing.com").email("contact@oldworldcomputing.com"));
    }

}
