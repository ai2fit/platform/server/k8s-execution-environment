package com.owc.singularity.server.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Home redirection to swagger api documentation
 */
@Controller
public class HomeController {

    @Value(value = "${springdoc.swagger-ui.path}")
    private String swaggerUiPath;

    @RequestMapping(value = "/")
    public String index() {
        return "redirect:" + swaggerUiPath;
    }
}
