package com.owc.singularity.server.configuration.format;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.ConverterFactory;
import org.springframework.lang.Nullable;


public class CustomEnumConverterFactory implements ConverterFactory<String, Enum<?>> {

    private final Map<Class<? extends Enum<?>>, Converter<String, ? extends Enum<?>>> converterMap = new ConcurrentHashMap<>();

    @SuppressWarnings("unchecked")
    @Override
    public <T extends Enum<?>> Converter<String, T> getConverter(Class<T> targetType) {
        return (Converter<String, T>) converterMap.computeIfAbsent(targetType, CustomEnumConverterFactory::createConverter);
    }

    private static <T extends Enum<?>> Converter<String, T> createConverter(Class<T> targetType) {
        CaseInsensitiveEnum annotation = targetType.getAnnotation(CaseInsensitiveEnum.class);
        if (annotation != null)
            return new CaseInsensitiveEnumConverter<>(targetType);
        else
            return new StringToEnum<>(targetType);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private static class StringToEnum<T extends Enum> implements Converter<String, T> {

        private final Class<T> enumType;

        StringToEnum(Class<T> enumType) {
            this.enumType = enumType;
        }

        @Override
        @Nullable
        public T convert(String source) {
            if (source.isEmpty()) {
                // It's an empty enum identifier: reset the enum value to null.
                return null;
            }
            return (T) Enum.valueOf(this.enumType, source.trim());
        }
    }
}
