package com.owc.singularity.server.service.job.k8s;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Sorts.*;

import java.io.IOException;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import com.mongodb.client.FindIterable;
import com.mongodb.client.model.Updates;
import com.owc.singularity.server.api.JobsApi;
import com.owc.singularity.server.model.Job;
import com.owc.singularity.server.model.QueryOptions;
import com.owc.singularity.server.model.QueryOptions.Direction;
import com.owc.singularity.server.model.module.ModuleInfo;
import com.owc.singularity.server.model.module.VersionNumber;
import com.owc.singularity.server.service.JobService;
import com.owc.singularity.server.service.ModuleService;
import com.owc.singularity.server.service.MongoConnection;
import com.owc.singularity.server.service.UserContext;
import com.owc.singularity.server.service.exception.AccessRightException;
import com.owc.singularity.server.service.job.k8s.scheduler.K8sScheduler;

@Service
public class K8sJobService implements JobService {

    private static Logger log = LogManager.getLogger();

    private final MongoConnection mongo;

    private final K8sScheduler scheduler;

    private final ModuleService moduleService;

    public K8sJobService(MongoConnection mongo, K8sScheduler scheduler, ModuleService moduleService) {
        this.mongo = mongo;
        this.scheduler = scheduler;
        this.moduleService = moduleService;
    }

    @Override
    public Stream<Job> all(UserContext context) throws AccessRightException, IOException {
        return StreamSupport.stream(mongo.getJobsCollection().find().sort(ascending("_id")).spliterator(), false);
    }

    @Override
    public Stream<Job> filter(UserContext context, QueryOptions options) throws AccessRightException, IOException {

        if (options.getCursor() != null) {
            // TODO
            if (!options.getSort().isEmpty())
                throw new IllegalArgumentException("Pagination with sorting is currently not supported.");
            if (options.getDirection() == Direction.Before)
                throw new IllegalArgumentException("Pagination backwards is currently not supported.");
        }

        FindIterable<Job> jobs;
        Bson filters = parseFilter(options.getCursor(), options.getFilter());
        if (filters == null) {
            jobs = mongo.getJobsCollection().find();
        } else {
            jobs = mongo.getJobsCollection().find(filters);
        }
        jobs.sort(parseSort(options.getSort()));
        jobs.limit(options.getSize());

        return StreamSupport.stream(jobs.spliterator(), false);
    }

    public static Bson parseFilter(String cursor, List<String> filter) {
        List<Bson> bsonFilters = filter.stream().map(s -> {
            String[] split = s.split(":");
            if (split.length != 2)
                throw new IllegalArgumentException("Filters need to have format like 'key:value'. Got: " + s);

            // default to regex for now
            return regex(split[0], split[1]);
        }).collect(Collectors.toList());

        if (cursor != null)
            bsonFilters.add(0, gt("_id", new ObjectId(cursor)));

        if (bsonFilters.isEmpty())
            return null;

        return and(bsonFilters);
    }

    public static Bson parseSort(List<String> sort) {
        List<Bson> bsonSorts = sort.stream().map(s -> {
            String[] split = s.split(",");
            if (split.length > 1) {
                switch (split[1]) {
                    case "asc":
                        return ascending(split[0]);
                    case "desc":
                        return descending(split[0]);
                    default:
                        throw new IllegalArgumentException("Sort direction " + split[1] + " is not known");
                }
            }

            return ascending(split[0]);
        }).collect(Collectors.toList());

        // always sort descending objectId to consistently resolve tiebreakers
        bsonSorts.add(descending("_id"));
        return orderBy(bsonSorts);
    }

    @Override
    public Job findById(UserContext context, String jobId) throws AccessRightException, IOException {
        return mongo.getJobsCollection().find(eq("id", jobId)).first();
    }

    @Override
    public void stopById(UserContext context, String jobId) throws AccessRightException, IOException {
        mongo.getJobsCollection().updateOne(eq("id", jobId), Updates.set("canceled", true));
        scheduler.terminate(jobId);
    }

    @Override
    public Job submit(UserContext context, JobsApi.JobSubmission submissionRequest) throws AccessRightException, IOException {
        Job job = new Job().id(UUID.randomUUID().toString())
                ._configuration(submissionRequest.getConfiguration())
                .name(submissionRequest.getName())
                .description(submissionRequest.getDescription())
                .submittedAt(Instant.now());

        validateJobSubmission(context, submissionRequest);

        mongo.getJobsCollection().insertOne(job);

        scheduler.submit(context, job.getId(), job.getConfiguration());

        return mongo.getJobsCollection().find(eq("id", job.getId())).first();
    }

    private void validateJobSubmission(UserContext context, JobsApi.JobSubmission submissionRequest) throws AccessRightException, IOException {
        Map<String, VersionNumber> wantedModules = submissionRequest.getConfiguration().getContext().getModules();
        if (wantedModules == null || wantedModules.isEmpty())
            return;

        List<ModuleInfo> availableModules = moduleService.all(context).collect(Collectors.toList());

        List<ModuleInfo> missingModules = wantedModules.entrySet()
                .stream()
                .map(ModuleInfo::of)
                .filter(moduleInfo -> !availableModules.contains(moduleInfo))
                .collect(Collectors.toList());

        if (!missingModules.isEmpty()) {
            throw new IllegalArgumentException("Modules: " + missingModules.toString() + " are missing. Please upload them before using.");
        }
    }
}
