package com.owc.singularity.server.service;

import java.io.IOException;
import java.util.stream.Stream;

import com.owc.singularity.server.model.NodeInformation;
import com.owc.singularity.server.model.QueryOptions;
import com.owc.singularity.server.service.exception.AccessRightException;

public interface NodeInformationService {

    Stream<NodeInformation> all(UserContext context) throws AccessRightException, IOException;

    Stream<NodeInformation> filter(UserContext context, QueryOptions options, Stream<NodeInformation> toFilter) throws AccessRightException, IOException;

    NodeInformation findById(UserContext context, String nodeId, Stream<NodeInformation> toFilter) throws AccessRightException, IOException;
}
