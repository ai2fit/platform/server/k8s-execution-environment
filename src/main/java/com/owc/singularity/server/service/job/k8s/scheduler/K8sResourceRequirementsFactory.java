package com.owc.singularity.server.service.job.k8s.scheduler;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import io.fabric8.kubernetes.api.model.Quantity;

@Service
public class K8sResourceRequirementsFactory {

    @Value("${service.job.scheduler.k8s.resource.cpu.request:2}")
    private String requestCpuQuantity;

    @Value("${service.job.scheduler.k8s.resource.cpu.default:2}")
    private String defaultCpuQuantity;

    @Value("${service.job.scheduler.k8s.resource.memory.default:8Gi}")
    private String defaultMemoryQuantity;

    @Value("${service.job.scheduler.k8s.resource.storage.default:#{null}}")
    private String defaultStorageQuantity;

    @Value("${service.job.scheduler.k8s.resource.cpu.id:cpu}")
    private String resourceCpuIdentifier;

    @Value("${service.job.scheduler.k8s.resource.memory.id:memory}")
    private String resourceMemoryIdentifier;

    @Value("${service.job.scheduler.k8s.resource.storage.id:ephemeral-storage}")
    private String resourceStorageIdentifier;

    public K8sResourceRequirementsFactory() {
        // empty constructor
    }

    public K8sResourceRequirementsBuilder withDefaults() {

        K8sResourceRequirementsBuilder builder = new K8sResourceRequirementsBuilder(requestCpuQuantity, resourceCpuIdentifier, resourceMemoryIdentifier,
                resourceStorageIdentifier);

        if (defaultCpuQuantity != null && !defaultCpuQuantity.isBlank())
            builder.withCpuLimit(new Quantity(defaultCpuQuantity));
        if (defaultMemoryQuantity != null && !defaultMemoryQuantity.isBlank())
            builder.withMemoryLimit(new Quantity(defaultMemoryQuantity));
        if (defaultStorageQuantity != null && !defaultStorageQuantity.isBlank())
            builder.withStorageLimit(new Quantity(defaultStorageQuantity));

        return builder;
    }
}
