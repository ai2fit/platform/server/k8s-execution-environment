package com.owc.singularity.server.service.worker;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.owc.singularity.server.model.MountConfiguration;
import com.owc.singularity.server.model.worker.WorkerMountConfiguration;
import com.owc.singularity.server.model.worker.WorkerMounts;

@Service
public class DefaultWorkerConfigurationFactory implements WorkerConfigurationFactory {

    @Value("${service.worker.mount.use.deferred.mounts:true}")
    private boolean useDeferredMounts;

    @Override
    public WorkerMounts createMountsConfiguration(List<MountConfiguration> mounts) {
        WorkerMounts result = new WorkerMounts();
        for (MountConfiguration jobMountConfig : mounts) {
            result.add(jobMountConfig.getPath(), convertToWorkerMountConfiguration(jobMountConfig, useDeferredMounts));
        }
        return result;
    }

    public WorkerMountConfiguration convertToWorkerMountConfiguration(MountConfiguration mountConfiguration, boolean deferred) {
        String type = mountConfiguration.getType();
        Map<String, String> options = mountConfiguration.getOptions() == null ? new HashMap<>() : new HashMap<>(mountConfiguration.getOptions());
        if (deferred) {
            options.put("delegate.type", type);
            return new WorkerMountConfiguration("deferred", options);
        }
        return new WorkerMountConfiguration(type, options);
    }
}
