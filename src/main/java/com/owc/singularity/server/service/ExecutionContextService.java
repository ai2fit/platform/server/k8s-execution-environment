package com.owc.singularity.server.service;

import java.io.IOException;
import java.util.List;
import java.util.stream.Stream;

import com.owc.singularity.server.model.ExecutionContext;
import com.owc.singularity.server.model.MountConfiguration;
import com.owc.singularity.server.model.QueryOptions;
import com.owc.singularity.server.service.exception.AccessRightException;

public interface ExecutionContextService {

    Stream<ExecutionContext> all(UserContext userContext);

    Stream<ExecutionContext> filter(UserContext context, QueryOptions options) throws AccessRightException, IOException;

    ExecutionContext findById(UserContext userContext, String executionContextId);

    ExecutionContext define(UserContext userContext, String name, List<MountConfiguration> mounts);
}
