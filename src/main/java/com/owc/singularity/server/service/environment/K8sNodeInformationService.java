package com.owc.singularity.server.service.environment;

import java.io.IOException;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

import com.owc.singularity.server.model.NodeInformation;
import com.owc.singularity.server.model.QueryOptions;
import com.owc.singularity.server.service.NodeInformationService;
import com.owc.singularity.server.service.UserContext;
import com.owc.singularity.server.service.exception.AccessRightException;
import com.owc.singularity.server.service.job.k8s.scheduler.K8sScheduler;


@Service
public class K8sNodeInformationService implements NodeInformationService {

    private final K8sScheduler scheduler;

    public K8sNodeInformationService(K8sScheduler scheduler) {
        this.scheduler = scheduler;
    }

    @Override
    public Stream<NodeInformation> all(UserContext context) throws AccessRightException, IOException {
        return scheduler.getNodeInformation();
    }

    @Override
    public Stream<NodeInformation> filter(UserContext context, QueryOptions options, Stream<NodeInformation> toFilter)
            throws AccessRightException, IOException {

        if (options.getCursor() != null) {
            // TODO
            throw new IllegalArgumentException("Pagination is currently not supported.");
        }

        if (options.getFilter() != null && !options.getFilter().isEmpty()) {
            // TODO
            throw new IllegalArgumentException("Filtering is currently not supported.");
        }

        if (options.getSort() != null && !options.getSort().isEmpty()) {
            // TODO
            throw new IllegalArgumentException("Sorting is currently not supported.");
        }

        return toFilter.limit(options.getSize());
    }

    @Override
    public NodeInformation findById(UserContext context, String nodeId, Stream<NodeInformation> toFilter) throws AccessRightException, IOException {
        return toFilter.filter(node -> node.getId().equals(nodeId)).findFirst().orElse(null);
    }
}
