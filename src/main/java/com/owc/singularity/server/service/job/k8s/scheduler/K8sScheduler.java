package com.owc.singularity.server.service.job.k8s.scheduler;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.owc.singularity.job.scheduling.Job;
import com.owc.singularity.job.scheduling.Scheduler;
import com.owc.singularity.server.api.controller.ModulesApiController;
import com.owc.singularity.server.model.ExecutionContext;
import com.owc.singularity.server.model.JobConfiguration;
import com.owc.singularity.server.model.NodeInformation;
import com.owc.singularity.server.model.NodeResources;
import com.owc.singularity.server.model.module.ModuleInfo;
import com.owc.singularity.server.service.ModuleService;
import com.owc.singularity.server.service.UserContext;
import com.owc.singularity.server.service.exception.AccessRightException;
import com.owc.singularity.server.service.worker.WorkerConfigurationFactory;

import io.fabric8.kubernetes.api.model.*;
import io.fabric8.kubernetes.api.model.batch.v1.JobBuilder;
import io.fabric8.kubernetes.client.KubernetesClient;

@Component
public class K8sScheduler implements Scheduler {

    private static Logger log = LogManager.getLogger();

    @Value("${service.job.scheduler.k8s.namespace:singularity}")
    private String namespace;

    @Value("${service.job.scheduler.k8s.labels.type:type}")
    private String typeLabel;

    @Value("${service.job.scheduler.k8s.labels.type.job:job}")
    private String jobTypeLabelValue;

    @Value("${service.job.scheduler.k8s.labels.type.cronjob:cronjob}")
    private String cronjobTypeLabelValue;

    @Value("${service.job.scheduler.k8s.labels.cronjob-name:cronjob-name}")
    private String cronjobNameLabel;

    @Value("${service.job.scheduler.k8s.mounts.extra.volume.name:#{null}}")
    private String extraMountVolumeName;

    @Value("${service.job.scheduler.k8s.mounts.extra.volume.path:#{null}}")
    private String extraMountVolumePath;

    @Value("${service.job.scheduler.k8s.mounts.extra.volume.local.path:#{null}}")
    private String extraMountVolumeLocalPath;

    @Value("${service.job.scheduler.k8s.mounts.extra.volume.readOnly:true}")
    private boolean extraMountVolumeReadOnly;

    @Value("${service.job.scheduler.k8s.image:registry.gitlab.com/ai2fit/platform/execution-worker:0.3.0}")
    private String workerImage;

    @Value("${service.job.scheduler.k8s.image.pull-secret-name:regcred}")
    private String imagePullSecretName;

    @Value("${service.job.scheduler.k8s.mounts.repository.volume.name:repository-mount}")
    private String repositoryMountVolumeName;

    @Value("${service.job.scheduler.k8s.mounts.repository.volume.path:/repository}")
    private String repositoryMountVolumePath;

    @Value("${service.job.scheduler.k8s.mounts.repository.volume.medium:Memory}")
    private String repositoryMountVolumeMedium;

    @Value("${service.job.scheduler.k8s.mounts.modules.volume.claim.storageClass:openebs-kernel-nfs}")
    private String modulesMountVolumeClaimStorageClass;

    @Value("${service.job.scheduler.k8s.mounts.modules.volume.claim.storage:1Gi}")
    private String modulesMountVolumeClaimStorage;

    @Value("${service.job.scheduler.k8s.mounts.modules.volume.type:emptyDir}")
    private String modulesMountVolumeType;

    @Value("${service.job.scheduler.k8s.mounts.modules.volume.accessMode:ReadWriteOnce}")
    private String modulesMountVolumeAccessMode;

    @Value("${service.job.scheduler.k8s.mounts.modules.volume.name:modules-mount}")
    private String modulesMountVolumeName;

    @Value("${service.job.scheduler.k8s.mounts.modules.volume.path:/modules}")
    private String modulesMountVolumePath;

    @Value("${service.job.scheduler.k8s.mounts.secret.volume.name:secret-mount}")
    private String secretMountVolumeName;

    @Value("${service.job.scheduler.k8s.mounts.secret.volume.path:/config}")
    private String secretMountVolumePath;

    @Value("${service.job.scheduler.k8s.mounts.secret.volume.fileName:repository_config.json}")
    private String repositoryConfigurationFileName;

    @Value("${service.job.scheduler.k8s.resource.cpu.id:cpu}")
    private String resourceCpuIdentifier;

    @Value("${service.job.scheduler.k8s.resource.memory.id:memory}")
    private String resourceMemoryIdentifier;

    @Value("${service.job.scheduler.k8s.resource.storage.id:ephemeral-storage}")
    private String resourceStorageIdentifier;

    @Value("${service.job.scheduler.singularity.log.level:INFO}")
    private String logLevel;

    @Value("${service.job.scheduler.singularity.java.opts:-XX:+UseZGC -XX:+AlwaysPreTouch -XX:+UseNUMA}")
    private String javaOpts;

    // if set to null this will use the external address used in the request
    @Value("${service.job.scheduler.k8s.internal.http:#{null}}")
    private String internalHttpAddress;

    private final KubernetesClient client;

    private final ObjectMapper objectMapper;

    private final ModuleService moduleService;

    private final WorkerConfigurationFactory workerConfigurationFactory;

    private final K8sResourceRequirementsFactory k8sResourceRequirementsFactory;

    public K8sScheduler(ObjectMapper objectMapper, KubernetesClient client, ModuleService moduleService, WorkerConfigurationFactory workerConfigurationFactory,
            K8sResourceRequirementsFactory k8sResourceRequirementsFactory) {
        this.client = client;
        this.objectMapper = objectMapper;
        this.moduleService = moduleService;
        this.workerConfigurationFactory = workerConfigurationFactory;
        this.k8sResourceRequirementsFactory = k8sResourceRequirementsFactory;
    }

    @Override
    public Stream<NodeInformation> getNodeInformation() {
        return client.nodes().list().getItems().stream().map(this::extractNodeInformation);
    }

    /*
     * TESTING ONLY
     */
    public Optional<io.fabric8.kubernetes.api.model.batch.v1.Job> getRawJob(String jobId) {
        io.fabric8.kubernetes.api.model.batch.v1.Job job = client.batch().v1().jobs().inNamespace(namespace).withName(jobId).get();
        if (job != null) {
            return Optional.ofNullable(job);
        }

        return Optional.ofNullable(null);
    }

    @Override
    public Optional<Job> getJob(String jobId) {
        io.fabric8.kubernetes.api.model.batch.v1.Job job = client.batch().v1().jobs().inNamespace(namespace).withName(jobId).get();
        return Optional.ofNullable(K8sJob.from(job));
    }

    @Override
    public Optional<Job> submit(UserContext userContext, String uuid, JobConfiguration configuration) throws IOException, AccessRightException {

        ExecutionContext context = configuration.getContext();
        if (context == null) {
            throw new IllegalArgumentException("Scheduling a job currently needs a complete context provided in the JobSubmissions configuration.");
        }

        if (context.getMounts() == null || context.getMounts().isEmpty()) {
            throw new IllegalArgumentException(
                    "Scheduling a job currently needs at least 1(!) MountConfiguration provided in the JobSubmissions configuration.");
        }

        List<ModuleInfo> wantedModules = validateModules(userContext, context);

        String secretName = uuid;

        client.secrets()
                .inNamespace(namespace)
                .resource(new SecretBuilder().withNewMetadata()
                        .withName(secretName)
                        .and()
                        .addToStringData(repositoryConfigurationFileName,
                                objectMapper.writeValueAsString(workerConfigurationFactory.createMountsConfiguration(context.getMounts())))
                        .build())
                .create();

        K8sResourceRequirementsBuilder resourceRequirementsBuilder = k8sResourceRequirementsFactory.withDefaults();
        NodeResources resources = context.getResources();
        if (resources != null) {
            if (resources.getCpu() != null)
                resourceRequirementsBuilder.withCpuLimit(new Quantity(resources.getCpu()));
            if (resources.getMemory() != null)
                resourceRequirementsBuilder.withMemoryLimit(new Quantity(resources.getMemory()));
            if (resources.getStorage() != null) {
                resourceRequirementsBuilder.withStorageLimit(new Quantity(resources.getStorage()));
            }
        }

        PodSpecBuilder podSpecBuilder = new PodSpecBuilder();
        String nodeId = context.getNodeId();
        if (nodeId != null) {
            List<String> availableNodeIds = client.nodes().list().getItems().stream().map(this::extractNodeId).collect(Collectors.toList());
            if (availableNodeIds.contains(nodeId))
                podSpecBuilder.withNodeSelector(Map.of("kubernetes.io/hostname", nodeId));
            else
                throw new IllegalArgumentException("Scheduling of job: " + uuid + " can't proceed since the specified node: " + nodeId
                        + " is not available. Available nodes are: " + availableNodeIds);
        }

        Volume repositoryVolume = new VolumeBuilder().withName(repositoryMountVolumeName)
                .withNewEmptyDir()
                .withMedium(repositoryMountVolumeMedium)
                .and()
                .build();
        Volume secretVolume = new VolumeBuilder().withName(secretMountVolumeName)
                .withSecret(new SecretVolumeSourceBuilder().withSecretName(secretName).withOptional(false).build())
                .build();
        List<Volume> volumes = new ArrayList<>(Arrays.asList(repositoryVolume, secretVolume));

        VolumeMount repositoryVolumeMount = new VolumeMountBuilder().withName(repositoryMountVolumeName).withMountPath(repositoryMountVolumePath).build();
        VolumeMount secretVolumeMount = new VolumeMountBuilder().withName(secretMountVolumeName)
                .withMountPath(secretMountVolumePath)
                .withReadOnly(true)
                .build();
        List<VolumeMount> volumeMounts = new ArrayList<>(Arrays.asList(repositoryVolumeMount, secretVolumeMount));

        if (!wantedModules.isEmpty()) {

            String moduleReplaceIdentifier = "MODULEID";

            Link linkTo = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(ModulesApiController.class).downloadModuleById(moduleReplaceIdentifier))
                    .withSelfRel();
            String modulesDownloadUrl = linkTo.toUri()
                    .toString()
                    .replace(moduleReplaceIdentifier,
                            "{" + wantedModules.stream().map(module -> module.getObjectId().toString()).collect(Collectors.joining(",")) + "}");

            if (internalHttpAddress != null && !internalHttpAddress.isEmpty()) {
                String oldModulesDownloadUrl = modulesDownloadUrl;
                String cleanedInternalHttpAddress = internalHttpAddress.endsWith("/") ? internalHttpAddress : internalHttpAddress + "/";

                modulesDownloadUrl = modulesDownloadUrl.replaceAll("^https?://[^/]*+/", cleanedInternalHttpAddress);

                log.trace("Replaced calling url {} with {} to support internal module download.", oldModulesDownloadUrl, modulesDownloadUrl);
            }

            Volume modulesVolume;
            switch (modulesMountVolumeType) {
                case "emptyDir":
                    modulesVolume = new VolumeBuilder().withName(modulesMountVolumeName)
                            .withNewEmptyDir()
                            .withMedium(repositoryMountVolumeMedium)
                            .and()
                            .build();
                    break;
                case "claim":
                    String modulesVolumePvcName = uuid + "-" + modulesMountVolumeName;
                    modulesVolume = new VolumeBuilder().withName(modulesMountVolumeName)
                            .withPersistentVolumeClaim(new PersistentVolumeClaimVolumeSource(modulesVolumePvcName, false))
                            .build();
                    client.persistentVolumeClaims()
                            .inNamespace(namespace)
                            .resource(new PersistentVolumeClaimBuilder().withNewMetadata()
                                    .withName(modulesVolumePvcName)
                                    .and()
                                    .withNewSpec()
                                    .withStorageClassName(modulesMountVolumeClaimStorageClass)
                                    .withAccessModes(modulesMountVolumeAccessMode)
                                    .withResources(new ResourceRequirementsBuilder()
                                            .withRequests(Collections.singletonMap("storage", new Quantity(modulesMountVolumeClaimStorage)))
                                            .build())
                                    .and()
                                    .build())
                            .create();
                    break;
                default:
                    throw new IllegalArgumentException("Config property 'service.job.scheduler.k8s.mounts.modules.volume.type' has unknown value "
                            + modulesMountVolumeType + ". Supported values: emptyDir, claim.");
            }


            VolumeMount modulesVolumeMount = new VolumeMountBuilder().withName(modulesMountVolumeName).withMountPath(modulesMountVolumePath).build();

            volumes.add(modulesVolume);
            volumeMounts.add(modulesVolumeMount);

            podSpecBuilder.withInitContainers(new ContainerBuilder().withName("modules")
                    .withImage("curlimages/curl")
                    .withArgs("-sS", "--remote-name-all", "--remote-header-name", "--output-dir", modulesMountVolumePath, modulesDownloadUrl)
                    .withVolumeMounts(modulesVolumeMount)
                    .build());
        }

        if (extraMountVolumeName != null && extraMountVolumePath != null && extraMountVolumeLocalPath != null) {
            volumes.add(new VolumeBuilder().withName(extraMountVolumeName).withNewHostPath().withPath(extraMountVolumeLocalPath).endHostPath().build());
            volumeMounts.add(
                    new VolumeMountBuilder().withMountPath(extraMountVolumePath).withName(extraMountVolumeName).withReadOnly(extraMountVolumeReadOnly).build());
        }

        podSpecBuilder.withImagePullSecrets(new LocalObjectReference(imagePullSecretName))
                .withVolumes(volumes)
                .withContainers(withWorker(uuid, configuration).addToEnv(new EnvVarBuilder().withName("JAVA_OPTS")
                        .withValue("-XX:ActiveProcessorCount=" + (int) resourceRequirementsBuilder.getMaxCpuCores() + " " + javaOpts)
                        .build()).withResources(resourceRequirementsBuilder.build()).withVolumeMounts(volumeMounts).build())
                .withRestartPolicy("Never");

        JobBuilder builder = new JobBuilder();
        io.fabric8.kubernetes.api.model.batch.v1.Job kubeJob = builder.withNewMetadata()
                .withName(uuid)
                .withLabels(Map.of(typeLabel, jobTypeLabelValue))
                .endMetadata()
                .withNewSpec()
                // number of retries
                .withBackoffLimit(0)
                // kubernetes job cleanup, should not matter since all job logs and status should be
                // in the database already
                .withTtlSecondsAfterFinished(600)
                .withNewTemplate()
                .withSpec(podSpecBuilder.build())
                .endTemplate()
                .endSpec()
                .build();

        log.debug("Creating job: {} with content: {}", uuid, kubeJob);

        kubeJob = client.batch().v1().jobs().inNamespace(namespace).resource(kubeJob).create();

        return Optional.ofNullable(K8sJob.from(kubeJob));
    }

    private WorkerContainerBuilder withWorker(String uuid, JobConfiguration configuration) {
        ExecutionContext context = configuration.getContext();
        String pipelinePath = configuration.getPipelinePath();
        String repositoryConfigPath = Path.of(secretMountVolumePath, repositoryConfigurationFileName).toAbsolutePath().toString();
        String pipelineLogLevel = context.getLogLevel() == null ? logLevel : context.getLogLevel();
        Map<String, String> pipelineVariables = context.getVariables() == null ? Collections.emptyMap() : context.getVariables();
        WorkerContainerBuilder workerContainer = new WorkerContainerBuilder(workerImage, uuid).withPipelinePath(pipelinePath)
                .withRepositoryConfigurationPath(repositoryConfigPath)
                .withLogLevel(pipelineLogLevel);
        pipelineVariables.forEach(workerContainer::withVariable);
        return workerContainer;
    }

    private List<ModuleInfo> validateModules(UserContext userContext, ExecutionContext context) throws AccessRightException, IOException {

        if (context.getModules() == null || context.getModules().isEmpty())
            return Collections.emptyList();

        List<ModuleInfo> availableModules = moduleService.all(userContext).collect(Collectors.toList());
        List<ModuleInfo> wantedModules = context.getModules()
                .entrySet()
                .stream()
                .map(entry -> availableModules.get(availableModules.indexOf(ModuleInfo.of(entry))))
                .collect(Collectors.toList());

        if (wantedModules.size() != context.getModules().size()) {
            List<ModuleInfo> missingModules = context.getModules()
                    .entrySet()
                    .stream()
                    .map(ModuleInfo::of)
                    .filter(Predicate.not(wantedModules::contains))
                    .collect(Collectors.toList());
            throw new IllegalArgumentException("Some required modules are not available. Missing modules: " + missingModules.toString());
        }
        return wantedModules;
    }


    public Stream<Job> streamJobs() {
        return client.batch().v1().jobs().inNamespace(namespace).list().getItems().stream().map(el -> K8sJob.from(el));
    }

    @Override
    public Optional<Job> terminate(String jobId) {
        client.batch().v1().jobs().inNamespace(namespace).withName(jobId).delete();
        return Optional.empty();
    }

    private NodeInformation extractNodeInformation(Node node) {
        NodeInformation nodeInformation = new NodeInformation().id(extractNodeId(node));
        NodeStatus status = node.getStatus();

        NodeResources allocatable = new NodeResources();
        NodeResources capacity = new NodeResources();
        if (status != null) {
            allocatable = extractNodeResources(status.getAllocatable());
            capacity = extractNodeResources(status.getCapacity());
        }
        nodeInformation.allocatable(allocatable).capacity(capacity);

        return nodeInformation;
    }

    private String extractNodeId(Node node) {
        return node.getMetadata().getName();
    }

    private NodeResources extractNodeResources(Map<String, Quantity> nodeAttributes) {
        NodeResources resources = new NodeResources();
        resources.cpu(Optional.ofNullable(nodeAttributes.get(resourceCpuIdentifier)).map(Quantity::toString).orElse(null));
        resources.memory(Optional.ofNullable(nodeAttributes.get(resourceMemoryIdentifier)).map(Quantity::toString).orElse(null));
        resources.storage(Optional.ofNullable(nodeAttributes.get(resourceStorageIdentifier)).map(Quantity::toString).orElse(null));
        return resources;
    }

    @Override
    public String getId() {
        ConfigMap configMap = client.configMaps()
                .inNamespace(namespace)
                .resource(new ConfigMapBuilder().withNewMetadata().withName("cluster-info").endMetadata().build())
                .get();

        if (configMap == null) {
            configMap = client.configMaps()
                    .inNamespace(namespace)
                    .resource(new ConfigMapBuilder().withNewMetadata()
                            .withName("cluster-info")
                            .endMetadata()
                            .addToData(Map.of("cluster-name", UUID.randomUUID().toString()))
                            .build())
                    .create();
        }

        return configMap.getData().get("cluster-name");
    }

    private static class WorkerContainerBuilder extends ContainerBuilder {

        public WorkerContainerBuilder(String workerImage, String containerName) {
            withImage(workerImage);
            withName(containerName);
        }

        public WorkerContainerBuilder withPipelinePath(String path) {
            addToEnv(new EnvVarBuilder().withName("SINGULARITY_PIPELINE_PATH").withValue(path).build(),
                    new EnvVarBuilder().withName("AI2FIT_PIPELINE_PATH").withValue(path).build());
            return this;
        }

        public WorkerContainerBuilder withRepositoryConfigurationPath(String repositoryConfigPath) {
            addToEnv(new EnvVarBuilder().withName("SINGULARITY_REPOSITORY_CONFIG_FILE_PATH").withValue(repositoryConfigPath).build(),
                    new EnvVarBuilder().withName("AI2FIT_REPOSITORY_CONFIG_PATH").withValue(repositoryConfigPath).build());
            return this;
        }

        public WorkerContainerBuilder withLogLevel(String logLevel) {
            addToEnv(new EnvVarBuilder().withName("SINGULARITY_LOG_LEVEL").withValue(logLevel).build(),
                    new EnvVarBuilder().withName("AI2FIT_LOG_LEVEL").withValue(logLevel).build());
            return this;
        }

        public WorkerContainerBuilder withVariable(String name, String value) {
            addToEnv(new EnvVarBuilder().withName("AI2FIT_VARIABLE_" + name).withValue(value).build());
            return this;
        }
    }
}
