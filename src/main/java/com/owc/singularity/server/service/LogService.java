package com.owc.singularity.server.service;

import java.io.IOException;

public interface LogService {

    String getLogsForExecution(UserContext context, String jobId, String filter, Integer lines, Boolean includeSourceTimestamps)
            throws RuntimeException, IOException;
}
