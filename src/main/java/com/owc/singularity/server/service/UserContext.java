package com.owc.singularity.server.service;

import java.time.Instant;

public class UserContext {

    private final String userId;
    private final Instant requestTimestamp;

    public UserContext(String userId, Instant requestTimestamp) {
        this.userId = userId;
        this.requestTimestamp = requestTimestamp;
    }

    public UserContext(String userId) {
        this(userId, Instant.now());
    }

    public String getUserId() {
        return userId;
    }

    public Instant getRequestTimestamp() {
        return requestTimestamp;
    }
}
