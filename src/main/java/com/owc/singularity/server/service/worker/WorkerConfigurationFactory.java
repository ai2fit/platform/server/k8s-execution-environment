package com.owc.singularity.server.service.worker;

import java.util.List;

import com.owc.singularity.server.model.MountConfiguration;
import com.owc.singularity.server.model.worker.WorkerMounts;

public interface WorkerConfigurationFactory {

    WorkerMounts createMountsConfiguration(List<MountConfiguration> mounts);
}
