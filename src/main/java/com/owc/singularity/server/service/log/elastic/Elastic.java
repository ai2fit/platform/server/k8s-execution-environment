package com.owc.singularity.server.service.log.elastic;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;

@Service
public class Elastic {

    @Value("${service.log.elastic.nodes:localhost:9200}")
    private String nodes;

    private ElasticsearchClient client = null;

    public ElasticsearchClient getClient() {
        if (client == null) {
            HttpHost[] hosts = parseNodeDefinitions(nodes);

            RestClient restClient = RestClient.builder(hosts).build();

            ElasticsearchTransport transport = new RestClientTransport(restClient, new JacksonJsonpMapper());
            client = new ElasticsearchClient(transport);
        }

        return client;
    }

    static HttpHost[] parseNodeDefinitions(String nodes) {
        String[] nodeDefinitions = nodes.split("[,;]");
        HttpHost[] hosts = new HttpHost[nodeDefinitions.length];
        for (int i = 0; i < hosts.length; i++) {
            String[] split = nodeDefinitions[i].split(":");
            hosts[i] = new HttpHost(split[0], Integer.parseInt(split[1]));
        }
        return hosts;
    }
}
