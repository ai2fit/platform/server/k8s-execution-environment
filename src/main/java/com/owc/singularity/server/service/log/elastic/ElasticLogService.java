package com.owc.singularity.server.service.log.elastic;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Value;

import com.owc.singularity.server.model.ElasticLogEntry;
import com.owc.singularity.server.service.LogService;
import com.owc.singularity.server.service.UserContext;

import co.elastic.clients.elasticsearch._types.FieldSort;
import co.elastic.clients.elasticsearch._types.SortOrder;
import co.elastic.clients.elasticsearch._types.query_dsl.BoolQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.MatchQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch._types.query_dsl.RegexpQuery;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;

public class ElasticLogService implements LogService {

    @Value("${service.log.elastic.property.jobId:kubernetes.labels.job-name}")
    String jobIdProperty;

    @Value("${service.log.elastic.index:logstash-*}")
    String elasticIndex;

    @Value("${service.log.elastic.property.timestamp:@timestamp}")
    String elasticTimestampProperty;

    @Value("${service.log.elastic.property.log:log}")
    String elasticLogProperty;

    @Value("${service.log.elastic.property.log.hasNewline:false}")
    boolean elasticLogPropertyHasNewLine;

    private final Elastic elastic;

    public ElasticLogService(Elastic elastic) {
        this.elastic = elastic;
    }

    @Override
    public String getLogsForExecution(UserContext context, String jobId, String filter, Integer lines, Boolean includeSourceTimestamps)
            throws RuntimeException, IOException {

        List<Query> boolQueries = new ArrayList<>(3);
        boolQueries.add(MatchQuery.of(t -> t.field(jobIdProperty).query(jobId))._toQuery());

        if (filter != null) {
            boolQueries.add(RegexpQuery.of(r -> r.field(elasticLogProperty).value(filter).caseInsensitive(true))._toQuery());
        }
        Query finalQuery = BoolQuery.of(b -> b.must(boolQueries))._toQuery();

        int size = lines == null ? 100 : lines;

        SearchResponse<ElasticLogEntry> response = elastic.getClient()
                .search(s -> s.index(elasticIndex)
                        .source(source -> source.filter(sourceFilter -> sourceFilter.includes(elasticTimestampProperty, elasticLogProperty)))
                        .query(finalQuery)
                        .sort(sort -> sort.field(FieldSort.of(fs -> fs.field(elasticTimestampProperty).order(SortOrder.Desc))))
                        .size(size), ElasticLogEntry.class);

        // return asc sorted log entries
        Stream<Hit<ElasticLogEntry>> hits = response.hits().hits().stream().sorted((entry1, entry2) -> entry1.sort().get(0).compareTo(entry2.sort().get(0)));

        StringBuilder builder = new StringBuilder();
        Consumer<Hit<ElasticLogEntry>> logLineBuilder;
        if (Boolean.TRUE.equals(includeSourceTimestamps)) {
            logLineBuilder = entry -> {
                builder.append(entry.source().timestamp);
                builder.append(" ");
                builder.append(entry.source().log);
            };
        } else {
            logLineBuilder = entry -> {
                builder.append(entry.source().log);
            };
        }

        if (elasticLogPropertyHasNewLine) {
            hits.forEach(logLineBuilder::accept);
        } else {
            hits.forEach(entry -> {
                logLineBuilder.accept(entry);
                builder.append("\n");
            });
        }

        return builder.toString();
    }
}
