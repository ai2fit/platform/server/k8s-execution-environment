package com.owc.singularity.server.service.log.mongo;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Sorts.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.bson.Document;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Indexes;
import com.mongodb.client.model.Projections;
import com.owc.singularity.server.service.LogService;
import com.owc.singularity.server.service.MongoConnection;
import com.owc.singularity.server.service.UserContext;

@Service
public class MongoDBLogService implements LogService, InitializingBean {

    @Value("${service.log.mongo.property.jobId:kubernetes.labels.job-name}")
    String jobIdProperty;

    @Value("${service.log.mongo.property.timestamp:timestamp}")
    String timestampProperty;

    @Value("${service.log.mongo.property.log:log}")
    String logProperty;

    @Value("${service.log.mongo.property.log.hasNewline:false}")
    boolean logPropertyHasNewLine;

    private final MongoConnection mongo;

    public MongoDBLogService(MongoConnection mongo) {
        this.mongo = mongo;
    }

    @Override
    public String getLogsForExecution(UserContext context, String jobId, String filter, Integer lines, Boolean includeSourceTimestamps)
            throws RuntimeException, IOException {

        int size = lines == null ? 100 : lines;

        MongoCursor<Document> result = mongo.getLogsCollection()
                .find(eq(jobIdProperty, jobId))
                .sort(descending(timestampProperty, "_id"))
                .projection(Projections.fields(Projections.include(logProperty, timestampProperty)))
                .limit(size)
                .iterator();
        List<Document> resultList = new ArrayList<>(size);

        while (result.hasNext()) {
            resultList.add(result.next());
        }

        StringBuilder builder = new StringBuilder();
        Consumer<Document> logLineBuilder;
        if (Boolean.TRUE.equals(includeSourceTimestamps)) {
            logLineBuilder = entry -> {
                builder.append(entry.getDate(timestampProperty));
                builder.append(" ");
                builder.append(entry.getString(logProperty));
            };
        } else {
            logLineBuilder = entry -> {
                builder.append(entry.getString(logProperty));
            };
        }

        if (logPropertyHasNewLine) {
            for (int i = resultList.size() - 1; i >= 0; i--) {
                logLineBuilder.accept(resultList.get(i));
            }
        } else {
            for (int i = resultList.size() - 1; i >= 0; i--) {
                logLineBuilder.accept(resultList.get(i));
                builder.append("\n");
            }
        }

        return builder.toString();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        mongo.getLogsCollection().createIndex(Indexes.ascending(jobIdProperty));
        mongo.getLogsCollection().createIndex(Indexes.descending(timestampProperty));
    }
}
