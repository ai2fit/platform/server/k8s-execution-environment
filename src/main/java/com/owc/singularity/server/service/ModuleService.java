package com.owc.singularity.server.service;

import java.io.File;
import java.io.IOException;
import java.util.stream.Stream;

import com.owc.singularity.server.model.QueryOptions;
import com.owc.singularity.server.model.module.ModuleInfo;
import com.owc.singularity.server.service.exception.AccessRightException;

import jakarta.servlet.http.HttpServletRequest;


public interface ModuleService {

    Stream<ModuleInfo> all(UserContext context) throws AccessRightException, IOException;

    Stream<ModuleInfo> filter(UserContext context, QueryOptions options) throws AccessRightException, IOException;

    ModuleInfo findById(UserContext context, String jobId) throws AccessRightException, IOException;

    ModuleInfo upload(UserContext context, HttpServletRequest request) throws AccessRightException, IOException;

    File download(UserContext context, String id) throws AccessRightException, IOException;

    void deleteById(UserContext context, String moduleId) throws AccessRightException, IOException;
}
