package com.owc.singularity.server.service.job.k8s.scheduler;

import java.util.Map;

import io.fabric8.kubernetes.api.model.Quantity;
import io.fabric8.kubernetes.api.model.ResourceRequirements;
import io.fabric8.kubernetes.api.model.ResourceRequirementsBuilder;

public class K8sResourceRequirementsBuilder {

    private final ResourceRequirementsBuilder builder;
    private final String requestCpuQuantity;
    private final String resourceCpuIdentifier;
    private final String resourceMemoryIdentifier;
    private final String resourceStorageIdentifier;

    private double maxCpuCores;
    private long maxMemoryBytes;

    protected K8sResourceRequirementsBuilder(String requestCpuQuantity, String resourceCpuIdentifier, String resourceMemoryIdentifier,
            String resourceStorageIdentifier) {
        this.builder = new ResourceRequirementsBuilder();
        this.requestCpuQuantity = requestCpuQuantity;
        this.resourceCpuIdentifier = resourceCpuIdentifier;
        this.resourceMemoryIdentifier = resourceMemoryIdentifier;
        this.resourceStorageIdentifier = resourceStorageIdentifier;
    }

    public K8sResourceRequirementsBuilder withCpuLimit(Quantity quantity) {
        Quantity requestQuantity = new Quantity(requestCpuQuantity);
        double requestCores = requestQuantity.getNumericalAmount().doubleValue();

        Quantity limitQuantity = new Quantity(Double.toString(Math.max(quantity.getNumericalAmount().doubleValue(), requestCores)));

        builder.addToRequests(Map.of(resourceCpuIdentifier, requestQuantity));
        builder.addToLimits(Map.of(resourceCpuIdentifier, limitQuantity));

        maxCpuCores = limitQuantity.getNumericalAmount().doubleValue();
        return this;
    }

    public double getMaxCpuCores() {
        return maxCpuCores;
    }

    public K8sResourceRequirementsBuilder withMemoryLimit(Quantity quantity) {
        builder.addToLimits(Map.of(resourceMemoryIdentifier, quantity));

        maxMemoryBytes = quantity.getNumericalAmount().longValue();
        return this;
    }

    public long getMaxMemoryBytes() {
        return maxMemoryBytes;
    }

    public K8sResourceRequirementsBuilder withStorageLimit(Quantity quantity) {
        builder.addToLimits(Map.of(resourceStorageIdentifier, quantity));

        return this;
    }

    public ResourceRequirements build() {
        return builder.build();
    }
}
