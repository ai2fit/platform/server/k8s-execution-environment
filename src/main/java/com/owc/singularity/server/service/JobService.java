package com.owc.singularity.server.service;

import java.io.IOException;
import java.util.stream.Stream;

import com.owc.singularity.server.api.JobsApi;
import com.owc.singularity.server.model.Job;
import com.owc.singularity.server.model.QueryOptions;
import com.owc.singularity.server.service.exception.AccessRightException;

public interface JobService {

    Stream<Job> all(UserContext context) throws AccessRightException, IOException;

    Stream<Job> filter(UserContext context, QueryOptions options) throws AccessRightException, IOException;

    Job findById(UserContext context, String jobId) throws AccessRightException, IOException;

    void stopById(UserContext context, String jobId) throws AccessRightException, IOException;

    Job submit(UserContext context, JobsApi.JobSubmission submissionRequest) throws AccessRightException, IOException;
}
