package com.owc.singularity.server.service.job.k8s.scheduler;

import java.time.ZonedDateTime;
import java.util.UUID;

import com.owc.singularity.job.scheduling.Job;
import com.owc.singularity.job.scheduling.JobExecution;
import com.owc.singularity.job.scheduling.JobStatus;

public class K8sJob implements Job {

    private final UUID uuid;
    private final JobExecution execution;

    public K8sJob(UUID uuid, JobExecution execution) {
        this.uuid = uuid;
        this.execution = execution;
    }

    @Override
    public UUID getUUID() {
        return uuid;
    }

    @Override
    public JobExecution getExecution() {
        return execution;
    }

    // https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/job-v1/
    public static K8sJob from(io.fabric8.kubernetes.api.model.batch.v1.Job job) {
        if (job == null)
            return null;

        // TODO: Job status != Pod/Container Status, how to map?
        // https://github.com/kubernetes/kubernetes/issues/95940
        // https://github.com/kubernetes/kubernetes/issues/97342
        io.fabric8.kubernetes.api.model.batch.v1.JobStatus k8sStatus = job.getStatus();
        String startTime = null;
        String completionTime = null;
        JobStatus status = JobStatus.PENDING;
        if (k8sStatus != null) {
            startTime = k8sStatus.getStartTime();
            completionTime = k8sStatus.getCompletionTime();

            if (existsAndGreaterZero(k8sStatus.getFailed())) {
                status = JobStatus.FAILED;
            } else if (existsAndGreaterZero(k8sStatus.getSucceeded())) {
                status = JobStatus.SUCCEEDED;
            } else if (existsAndGreaterZero(k8sStatus.getActive())) {
                status = JobStatus.RUNNING;
            }
        }

        JobExecution execution = new JobExecution(status, startTime == null ? null : ZonedDateTime.parse(startTime),
                completionTime == null ? null : ZonedDateTime.parse(completionTime));

        return new K8sJob(UUID.fromString(job.getMetadata().getName()), execution);
    }

    private static boolean existsAndGreaterZero(Integer integer) {
        return integer != null && integer > 0;
    }
}
