package com.owc.singularity.server.service;

import java.io.IOException;
import java.util.stream.Stream;

import com.owc.singularity.server.model.ExecutionEnvironment;
import com.owc.singularity.server.model.QueryOptions;
import com.owc.singularity.server.service.exception.AccessRightException;

public interface ExecutionEnvironmentService {

    Stream<ExecutionEnvironment> all(UserContext context) throws AccessRightException, IOException;

    Stream<ExecutionEnvironment> filter(UserContext context, QueryOptions options) throws AccessRightException, IOException;

    ExecutionEnvironment self(UserContext context) throws AccessRightException, IOException;

    ExecutionEnvironment findById(UserContext context, String environmentId) throws AccessRightException, IOException;
}
