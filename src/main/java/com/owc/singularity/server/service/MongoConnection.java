package com.owc.singularity.server.service;

import org.bson.Document;
import org.bson.UuidRepresentation;
import org.mongojack.JacksonMongoCollection;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mongodb.BasicDBObject;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoException;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.owc.singularity.server.model.Job;
import com.owc.singularity.server.model.module.ModuleInfo;

@Service("mongoConnection123")
public class MongoConnection {

    @Value("${service.job.db.mongo.hostname:mongo}")
    private String hostname;

    @Value("${service.job.db.mongo.port:27017}")
    private String port;

    @Value("${service.job.db.mongo.username:root}")
    private String username;

    @Value("${service.job.db.mongo.password:root}")
    private String password;

    @Value("${service.job.db.mongo.authdatabase:}")
    private String authDatabase;

    @Value("${service.job.db.mongo.database:execution-controller}")
    private String database;

    @Value("${service.job.db.mongo.collection.job:jobs}")
    private String jobCollection;

    @Value("${service.job.db.mongo.collection.cronjob:cron-jobs}")
    private String cronjobCollection;

    @Value("${service.job.db.mongo.collection.modules:modules}")
    private String modulesCollection;

    @Value("${service.job.db.mongo.collection.logs:logs}")
    private String logsCollection;

    private MongoClient client = null;

    private MongoClient getClient() {
        if (client == null) {
            String connectionString;
            if (username.isEmpty() && password.isEmpty()) {
                connectionString = "mongodb://" + hostname + ":" + port;
            } else {
                connectionString = "mongodb://" + username + ":" + password + "@" + hostname + ":" + port;
            }

            if (!authDatabase.isEmpty())
                connectionString += "/?authSource=" + authDatabase;

            MongoClientSettings settings = MongoClientSettings.builder().applyConnectionString(new ConnectionString(connectionString)).build();
            client = MongoClients.create(settings);
        }

        return client;
    }

    public boolean testConnection() {
        BasicDBObject ping = new BasicDBObject("ping", "1");
        try {
            getClient().getDatabase(database).runCommand(ping);
            return true;
        } catch (MongoException e) {
            return false;
        }
    }

    public MongoCollection<Job> getJobsCollection() {
        return JacksonMongoCollection.builder().build(getClient(), database, jobCollection, Job.class, UuidRepresentation.STANDARD);
    }

    public MongoCollection<ModuleInfo> getModulesCollection() {
        return JacksonMongoCollection.builder().build(getClient(), database, modulesCollection, ModuleInfo.class, UuidRepresentation.STANDARD);
    }

    public MongoCollection<Document> getLogsCollection() {
        return getClient().getDatabase(database).getCollection(logsCollection);
    }
}
