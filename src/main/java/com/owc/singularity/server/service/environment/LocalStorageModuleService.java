package com.owc.singularity.server.service.environment;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Sorts.*;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.time.Instant;
import java.util.jar.JarInputStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mongodb.client.FindIterable;
import com.owc.singularity.server.model.QueryOptions;
import com.owc.singularity.server.model.QueryOptions.Direction;
import com.owc.singularity.server.model.module.ModuleInfo;
import com.owc.singularity.server.model.module.ModuleManifest;
import com.owc.singularity.server.service.ModuleService;
import com.owc.singularity.server.service.MongoConnection;
import com.owc.singularity.server.service.UserContext;
import com.owc.singularity.server.service.exception.AccessRightException;
import com.owc.singularity.server.service.job.k8s.K8sJobService;

import jakarta.servlet.http.HttpServletRequest;


@Service
public class LocalStorageModuleService implements ModuleService {

    private static Logger log = LogManager.getLogger();

    private final MongoConnection mongo;

    @Value("${service.module.mounts.modules.storage.path:/storage}")
    private String modulesStoragePath;

    public LocalStorageModuleService(MongoConnection mongo) {
        this.mongo = mongo;
    }

    @Override
    public Stream<ModuleInfo> all(UserContext context) throws AccessRightException, IOException {
        return StreamSupport.stream(mongo.getModulesCollection().find().sort(ascending("_id")).spliterator(), false);
    }

    @Override
    public Stream<ModuleInfo> filter(UserContext context, QueryOptions options) throws AccessRightException, IOException {
        if (options.getCursor() != null) {
            // TODO
            if (!options.getSort().isEmpty())
                throw new IllegalArgumentException("Pagination with sorting is currently not supported.");
            if (options.getDirection() == Direction.Before)
                throw new IllegalArgumentException("Pagination backwards is currently not supported.");
        }

        FindIterable<ModuleInfo> moduleInfos;
        Bson filters = K8sJobService.parseFilter(options.getCursor(), options.getFilter());
        if (filters == null) {
            moduleInfos = mongo.getModulesCollection().find();
        } else {
            moduleInfos = mongo.getModulesCollection().find(filters);
        }
        moduleInfos.sort(K8sJobService.parseSort(options.getSort()));
        moduleInfos.limit(options.getSize());

        return StreamSupport.stream(moduleInfos.spliterator(), false);
    }

    @Override
    public ModuleInfo findById(UserContext context, String moduleId) throws AccessRightException, IOException {
        return mongo.getModulesCollection().find(eq("_id", new ObjectId(moduleId))).first();
    }

    @Override
    public ModuleInfo upload(UserContext context, HttpServletRequest request) throws AccessRightException, IOException {

        if (!Files.exists(Paths.get(modulesStoragePath))) {
            throw new IOException("Can't save modules because Path does not exist '" + modulesStoragePath
                    + "'. Please make sure to provide an appropriate storage location or volume mount at the specified path.");
        }

        Path tmpPath = Files.createTempFile("moduleUpload", ".jar");

        try {
            try (InputStream inputStream = request.getInputStream()) {
                Files.copy(inputStream, tmpPath, StandardCopyOption.REPLACE_EXISTING);
            }

            ModuleInfo info;
            try (JarInputStream inputStream = new JarInputStream(Files.newInputStream(tmpPath, StandardOpenOption.READ))) {
                info = ModuleManifest.extractModuleInfo(inputStream);
            }

            log.debug("Trying to register module: {}", info);

            if (!ModuleManifest.isModule(info)) {
                log.error("Not a module: {}", info);
                throw new IllegalArgumentException();
            }

            ModuleInfo existingModuleInfo = mongo.getModulesCollection()
                    .find(and(eq("vendor", info.getVendor()), eq("moduleKey", info.getModuleKey()), eq("name", info.getName()),
                            eq("version", info.getVersion().toString())))
                    .first();

            if (existingModuleInfo != null) {
                log.error("Found existing module in database: {}", existingModuleInfo);
                throw new IllegalArgumentException();
            }

            Path outputPath = Paths.get(modulesStoragePath, info.toFileName());
            info.setFilePath(info.toFileName());
            info.setUploadedAt(Instant.now());

            Files.move(tmpPath, outputPath, StandardCopyOption.REPLACE_EXISTING);
            mongo.getModulesCollection().insertOne(info);

            log.debug("Registered module: {}", info);

            return info;
        } finally {
            Files.deleteIfExists(tmpPath);
        }
    }

    @Override
    public File download(UserContext context, String id) throws AccessRightException, IOException {
        ModuleInfo info = findById(context, id);

        return Paths.get(modulesStoragePath, info.getFilePath()).toFile();
    }

    @Override
    public void deleteById(UserContext context, String moduleId) throws AccessRightException, IOException {
        ModuleInfo info = findById(context, moduleId);
        if (info == null) {
            throw new IllegalArgumentException("Module with id " + moduleId + " does not exist.");
        }

        try {
            Files.delete(Paths.get(modulesStoragePath, info.toFileName()));
        } catch (IOException ignored) {
            log.warn("Deleting local jar of module {} failed with exception: {}", info, ignored);
        }

        mongo.getModulesCollection().deleteOne(eq("_id", info.getObjectId()));
    }
}
