package com.owc.singularity.server.service.environment;

import java.io.IOException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.owc.singularity.job.scheduling.Scheduler;
import com.owc.singularity.server.model.ExecutionEnvironment;
import com.owc.singularity.server.model.QueryOptions;
import com.owc.singularity.server.service.ExecutionEnvironmentService;
import com.owc.singularity.server.service.NodeInformationService;
import com.owc.singularity.server.service.UserContext;
import com.owc.singularity.server.service.exception.AccessRightException;

@Service
public class K8sExecutionEnvironmentService implements ExecutionEnvironmentService {

    @Value("${environment.id:#{null}}")
    private String environmentId;

    private final Scheduler scheduler;
    private final NodeInformationService nodeInformationService;

    public K8sExecutionEnvironmentService(Scheduler scheduler, NodeInformationService nodeInformationService) {
        this.nodeInformationService = nodeInformationService;
        this.scheduler = scheduler;
    }

    @Override
    public Stream<ExecutionEnvironment> all(UserContext context) throws AccessRightException, IOException {
        return Stream.of(getSelf(context));
    }

    @Override
    public Stream<ExecutionEnvironment> filter(UserContext context, QueryOptions options) throws AccessRightException, IOException {
        if (options.getCursor() != null) {
            // TODO
            throw new IllegalArgumentException("Pagination is currently not supported.");
        }

        if (options.getFilter() != null && !options.getFilter().isEmpty()) {
            // TODO
            throw new IllegalArgumentException("Filtering is currently not supported.");
        }

        if (options.getSort() != null && !options.getSort().isEmpty()) {
            // TODO
            throw new IllegalArgumentException("Sorting is currently not supported.");
        }

        return Stream.of(getSelf(context)).limit(options.getSize());
    }

    @Override
    public ExecutionEnvironment self(UserContext context) throws AccessRightException, IOException {
        return getSelf(context);
    }

    @Override
    public ExecutionEnvironment findById(UserContext context, String environmentId) throws AccessRightException, IOException {
        if (environmentId.equals("self") || environmentId.equals(getEnvironmentId()))
            return getSelf(context);

        return null;
    }

    private ExecutionEnvironment getSelf(UserContext context) throws AccessRightException, IOException {
        return new ExecutionEnvironment().id(getEnvironmentId()).nodes(nodeInformationService.all(context).collect(Collectors.toList()));
    }

    private String getEnvironmentId() {
        if (environmentId == null)
            return scheduler.getId();

        return environmentId;
    }
}
