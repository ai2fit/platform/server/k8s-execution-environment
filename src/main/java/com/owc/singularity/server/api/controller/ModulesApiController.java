package com.owc.singularity.server.api.controller;

import java.io.File;
import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.AbstractResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.owc.singularity.server.api.ModulesApi;
import com.owc.singularity.server.model.QueryOptions;
import com.owc.singularity.server.model.module.ModuleInfo;
import com.owc.singularity.server.service.ModuleService;
import com.owc.singularity.server.service.UserContext;
import com.owc.singularity.server.service.exception.AccessRightException;

import jakarta.servlet.http.HttpServletRequest;

@RestController
public class ModulesApiController implements ModulesApi {

    private static final Logger log = LogManager.getLogger(ModulesApiController.class);

    private final HttpServletRequest request;

    private final ModuleService moduleService;

    public ModulesApiController(HttpServletRequest request, ModuleService moduleService) {
        this.request = request;
        this.moduleService = moduleService;
    }

    @Override
    public ResponseEntity<List<ModuleInfo>> getAllModules(QueryOptions options) {
        String accept = request.getHeader("Accept");
        if (accept != null && (accept.contains("*/*") || accept.contains("application/json"))) {
            try {
                Principal userPrincipal = request.getUserPrincipal();
                UserContext context = new UserContext("user");
                try {
                    List<ModuleInfo> modules = moduleService.filter(context, options).collect(Collectors.toList());
                    return ResponseEntity.ok(modules);
                } catch (AccessRightException e) {
                    log.warn("Unauthorized access", e);
                    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
                }
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @Override
    public ResponseEntity<ModuleInfo> getModuleById(String moduleId) {
        String accept = request.getHeader("Accept");
        if (accept != null && (accept.contains("*/*") || accept.contains("application/json"))) {
            try {
                Principal userPrincipal = request.getUserPrincipal();
                UserContext context = new UserContext("user");
                try {
                    ModuleInfo module = moduleService.findById(context, moduleId);
                    if (module != null)
                        return ResponseEntity.ok(module);

                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                } catch (AccessRightException e) {
                    log.warn("Unauthorized access", e);
                    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
                }
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @Override
    public ResponseEntity<AbstractResource> downloadModuleById(String moduleId) {
        String accept = request.getHeader("Accept");
        if (accept != null
                && (accept.contains("*/*") || accept.contains("application/java-archive") || accept.contains(MediaType.APPLICATION_OCTET_STREAM_VALUE))) {
            try {
                Principal userPrincipal = request.getUserPrincipal();
                UserContext context = new UserContext("user");
                try {
                    File module = moduleService.download(context, moduleId);
                    if (module != null)
                        return ResponseEntity.ok()
                                .contentType(accept.contains(MediaType.APPLICATION_OCTET_STREAM_VALUE) ? MediaType.APPLICATION_OCTET_STREAM
                                        : MediaType.valueOf("application/java-archive"))
                                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + module.getName() + "\"")
                                .header(HttpHeaders.CONTENT_LENGTH, module.length() + "")
                                .body(new FileSystemResource(module));

                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                } catch (AccessRightException e) {
                    log.warn("Unauthorized access", e);
                    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
                }
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/java-archive", e);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @Override
    public ResponseEntity<ModuleInfo> uploadModule(HttpServletRequest request) {
        String accept = request.getHeader("Accept");
        if (accept != null && (accept.contains("*/*") || accept.contains("application/json"))) {
            try {
                Principal userPrincipal = request.getUserPrincipal();
                UserContext context = new UserContext("user");
                try {
                    ModuleInfo module = moduleService.upload(context, request);
                    if (module != null)
                        return ResponseEntity.ok(module);

                    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
                } catch (AccessRightException e) {
                    log.warn("Unauthorized access", e);
                    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
                }
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @Override
    public ResponseEntity<ModuleInfo> deleteModuleById(String moduleId) {

        try {
            Principal userPrincipal = request.getUserPrincipal();
            UserContext context = new UserContext("user");
            try {
                moduleService.deleteById(context, moduleId);

                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } catch (IllegalArgumentException e) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } catch (AccessRightException e) {
                log.warn("Unauthorized access", e);
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
        } catch (IOException e) {
            log.error("Couldn't serialize response for content type application/json", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
