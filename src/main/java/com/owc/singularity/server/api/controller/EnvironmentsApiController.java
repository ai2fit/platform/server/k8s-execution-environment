package com.owc.singularity.server.api.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.owc.singularity.server.api.EnvironmentsApi;
import com.owc.singularity.server.model.ExecutionEnvironment;
import com.owc.singularity.server.model.NodeInformation;
import com.owc.singularity.server.model.QueryOptions;
import com.owc.singularity.server.service.ExecutionEnvironmentService;
import com.owc.singularity.server.service.NodeInformationService;
import com.owc.singularity.server.service.UserContext;
import com.owc.singularity.server.service.exception.AccessRightException;

import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.servlet.http.HttpServletRequest;


@RestController
public class EnvironmentsApiController implements EnvironmentsApi {

    private static final Logger log = LogManager.getLogger(EnvironmentsApiController.class);

    private final HttpServletRequest request;

    private final ExecutionEnvironmentService executionEnvironmentService;

    private final NodeInformationService nodeInformationService;

    public EnvironmentsApiController(HttpServletRequest request, ExecutionEnvironmentService executionEnvironmentService,
            NodeInformationService nodeInformationService) {
        this.request = request;
        this.executionEnvironmentService = executionEnvironmentService;
        this.nodeInformationService = nodeInformationService;
    }

    @Override
    public ResponseEntity<List<ExecutionEnvironment>> getAllEnvironments(QueryOptions options) {
        String accept = request.getHeader("Accept");
        if (accept != null && (accept.contains("*/*") || accept.contains("application/json"))) {
            try {

                Principal userPrincipal = request.getUserPrincipal();
                UserContext context = new UserContext("user");
                try {
                    return ResponseEntity.ok(executionEnvironmentService.filter(context, options).collect(Collectors.toList()));
                } catch (AccessRightException e) {
                    log.warn("Unauthorized access", e);
                    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
                }
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @Override
    public ResponseEntity<ExecutionEnvironment> getEnvironmentById(
            @Parameter(in = ParameterIn.PATH, description = "environment id", required = true, schema = @Schema()) @PathVariable("environmentId") String environmentId) {
        String accept = request.getHeader("Accept");
        if (accept != null && (accept.contains("*/*") || accept.contains("application/json"))) {
            try {

                Principal userPrincipal = request.getUserPrincipal();
                UserContext context = new UserContext("user");
                try {
                    ExecutionEnvironment environment = executionEnvironmentService.findById(context, environmentId);
                    if (environment != null)
                        return ResponseEntity.ok(environment);

                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                } catch (AccessRightException e) {
                    log.warn("Unauthorized access", e);
                    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
                }
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<NodeInformation>> getAllNodes(
            @Parameter(in = ParameterIn.PATH, description = "environment id", required = true, schema = @Schema()) @PathVariable("environmentId") String environmentId,
            QueryOptions options) {
        String accept = request.getHeader("Accept");
        if (accept != null && (accept.contains("*/*") || accept.contains("application/json"))) {
            try {

                Principal userPrincipal = request.getUserPrincipal();
                UserContext context = new UserContext("user");
                try {
                    ExecutionEnvironment environment = executionEnvironmentService.findById(context, environmentId);
                    if (environment != null) {
                        return ResponseEntity.ok(nodeInformationService.filter(context, options, environment.getNodes().stream()).collect(Collectors.toList()));
                    }

                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                } catch (AccessRightException e) {
                    log.warn("Unauthorized access", e);
                    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
                }
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<NodeInformation> getNodeById(
            @Parameter(in = ParameterIn.PATH, description = "environment id", required = true, schema = @Schema()) @PathVariable("environmentId") String environmentId,
            @Parameter(in = ParameterIn.PATH, description = "node id", required = true, schema = @Schema()) @PathVariable("nodeId") String nodeId) {
        String accept = request.getHeader("Accept");
        if (accept != null && (accept.contains("*/*") || accept.contains("application/json"))) {
            try {

                Principal userPrincipal = request.getUserPrincipal();
                UserContext context = new UserContext("user");
                try {
                    ExecutionEnvironment environment = executionEnvironmentService.findById(context, environmentId);
                    if (environment != null) {
                        return ResponseEntity.ok(nodeInformationService.findById(context, nodeId, environment.getNodes().stream()));
                    }

                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                } catch (AccessRightException e) {
                    log.warn("Unauthorized access", e);
                    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
                }
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
