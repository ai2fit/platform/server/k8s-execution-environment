package com.owc.singularity.server.api.controller;


import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.owc.singularity.server.api.JobsApi;
import com.owc.singularity.server.model.Job;
import com.owc.singularity.server.model.QueryOptions;
import com.owc.singularity.server.service.JobService;
import com.owc.singularity.server.service.LogService;
import com.owc.singularity.server.service.UserContext;
import com.owc.singularity.server.service.exception.AccessRightException;

import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;


@RestController
public class JobsApiController implements JobsApi {

    private static final Logger log = LogManager.getLogger(JobsApiController.class);

    private final HttpServletRequest request;

    private final JobService jobService;

    private final LogService logService;

    public JobsApiController(HttpServletRequest request, JobService jobService, LogService logService) {
        this.request = request;
        this.jobService = jobService;
        this.logService = logService;
    }

    public ResponseEntity<List<Job>> getAllJobs(QueryOptions options) {
        String accept = request.getHeader("Accept");
        if (accept != null && (accept.contains("*/*") || accept.contains("application/json"))) {
            try {
                Principal userPrincipal = request.getUserPrincipal();
                UserContext context = new UserContext("user");
                try {
                    List<Job> jobs = jobService.filter(context, options).collect(Collectors.toList());
                    return ResponseEntity.ok(jobs);
                } catch (AccessRightException e) {
                    log.warn("Unauthorized access", e);
                    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
                }
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Job> getJobById(
            @Parameter(in = ParameterIn.PATH, description = "ID of job to return", required = true, schema = @Schema()) @PathVariable("jobId") String jobId) {
        String accept = request.getHeader("Accept");
        if (accept != null && (accept.contains("*/*") || accept.contains("application/json"))) {
            try {

                Principal userPrincipal = request.getUserPrincipal();
                UserContext context = new UserContext("user");
                try {
                    Job job = jobService.findById(context, jobId);
                    if (job != null)
                        return ResponseEntity.ok(job);

                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                } catch (AccessRightException e) {
                    log.warn("Unauthorized access", e);
                    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
                }
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<String> getJobLogById(
            @Parameter(in = ParameterIn.PATH, description = "ID of job", required = true, schema = @Schema()) @PathVariable("jobId") String jobId,
            @Parameter(in = ParameterIn.QUERY, description = "Regex filter for log lines", schema = @Schema()) @Valid @RequestParam(value = "filter", required = false) String filter,
            @Parameter(in = ParameterIn.QUERY, description = "Number of log lines to filter", schema = @Schema()) @Valid @RequestParam(value = "lines", required = false) Integer lines,
            @Parameter(in = ParameterIn.QUERY, description = "Include source timestamps", schema = @Schema()) @Valid @RequestParam(value = "includeSourceTimestamps", required = false) Boolean includeSourceTimestamps) {
        String accept = request.getHeader("Accept");

        if (accept != null && (accept.contains("*/*") || accept.contains("text/plain"))) {
            try {

                Principal userPrincipal = request.getUserPrincipal();
                UserContext context = new UserContext("user");

                String logs = logService.getLogsForExecution(context, jobId, filter, lines, includeSourceTimestamps);

                return ResponseEntity.ok(logs);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> stopJobById(
            @Parameter(in = ParameterIn.PATH, description = "ID of job to stop", required = true, schema = @Schema()) @PathVariable("jobId") String jobId) {
        String accept = request.getHeader("Accept");
        if (accept != null && (accept.contains("*/*") || accept.contains("application/json"))) {
            try {

                Principal userPrincipal = request.getUserPrincipal();
                UserContext context = new UserContext("user");
                try {
                    jobService.stopById(context, jobId);
                    return ResponseEntity.noContent().build();
                } catch (AccessRightException e) {
                    log.warn("Unauthorized access", e);
                    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
                }
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Job> submitJob(
            @Parameter(in = ParameterIn.DEFAULT, description = "", schema = @Schema()) @Valid @RequestBody JobsApi.JobSubmission body) {
        String accept = request.getHeader("Accept");
        if (accept != null && (accept.contains("*/*") || accept.contains("application/json"))) {
            try {
                Principal userPrincipal = request.getUserPrincipal();
                UserContext context = new UserContext("user");
                try {
                    Job job = jobService.submit(context, body);
                    return new ResponseEntity<>(job, HttpStatus.CREATED);
                } catch (AccessRightException e) {
                    log.warn("Unauthorized access", e);
                    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
                }
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
