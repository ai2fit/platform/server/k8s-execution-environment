package com.owc.singularity.job.scheduling;

import java.util.List;

public interface CronJob extends Job {

    @Override
    default boolean hasSchedule() {
        return true;
    }

    String getSchedule();

    List<JobExecution> getExecutions();
}
