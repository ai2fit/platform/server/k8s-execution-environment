package com.owc.singularity.job.scheduling;

public enum JobStatus {
    PENDING, RUNNING, SUCCEEDED, FAILED, UNKNOWN
}
