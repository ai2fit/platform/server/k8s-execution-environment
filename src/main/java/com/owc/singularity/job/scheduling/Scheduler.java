package com.owc.singularity.job.scheduling;

import java.io.IOException;
import java.util.Optional;
import java.util.stream.Stream;

import com.owc.singularity.server.model.JobConfiguration;
import com.owc.singularity.server.model.NodeInformation;
import com.owc.singularity.server.service.UserContext;
import com.owc.singularity.server.service.exception.AccessRightException;

public interface Scheduler {

    public Stream<NodeInformation> getNodeInformation();

    public Optional<Job> getJob(String jobId);

    public Optional<Job> submit(UserContext context, String uuid, JobConfiguration configuration) throws IOException, AccessRightException;

    public Optional<Job> terminate(String jobId);

    public Stream<Job> streamJobs();

    public String getId();
}
