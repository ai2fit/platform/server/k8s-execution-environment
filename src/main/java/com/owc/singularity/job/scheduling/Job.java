package com.owc.singularity.job.scheduling;

import java.util.UUID;

public interface Job {

    default boolean hasSchedule() {
        return false;
    }

    UUID getUUID();

    JobExecution getExecution();
}
