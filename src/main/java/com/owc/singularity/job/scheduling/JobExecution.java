package com.owc.singularity.job.scheduling;

import java.time.ZonedDateTime;

public class JobExecution {

    private final JobStatus status;
    private final ZonedDateTime startTime;
    private final ZonedDateTime endTime;

    public JobExecution(JobStatus status, ZonedDateTime startTime, ZonedDateTime endTime) {
        this.status = status;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public JobStatus getStatus() {
        return status;
    }

    public ZonedDateTime getStartTime() {
        return this.startTime;
    }

    public ZonedDateTime getEndTime() {
        return this.endTime;
    }
}
