package com.owc.singularity.server.service.log.elastic;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.awaitility.Awaitility;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.owc.singularity.server.model.ElasticLogEntry;
import com.owc.singularity.server.service.SystemPropertyActiveProfileResolver;

import co.elastic.clients.elasticsearch._types.query_dsl.MatchQuery;
import co.elastic.clients.elasticsearch.core.IndexRequest;
import co.elastic.clients.json.JsonData;

@SpringBootTest
@ActiveProfiles(value = "integration-test-local", resolver = SystemPropertyActiveProfileResolver.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ElasticLogServiceTest {

    @Autowired
    Elastic elastic;

    @Autowired
    ElasticLogService elasticLogService;

    final String index = "logstash-2022-19-10-test";

    final String jobName = UUID.randomUUID().toString();
    final String executionId = UUID.randomUUID().toString();
    final String anotherExecutionId = UUID.randomUUID().toString();
    final String logMessage = "Some log message";

    @BeforeAll
    void insertLogData() throws Exception {
        elastic.getClient().indices().create(c -> c.index(index));
        elastic.getClient().indices().putMapping(p -> p.index(index).properties("@timestamp", b -> b.date(d -> d)));

        insertIntoElasticIndex("{\"@timestamp\": \"2022-10-19T13:55:32Z\", \"level\": \"warn\", \"log\": \"" + logMessage
                + "\", \"kubernetes\": {\"pod_id\": \"" + executionId + "\", \"labels\": {\"job-name\": \"" + jobName + "\"}}}");

        insertIntoElasticIndex("{\"@timestamp\": \"2022-10-19T13:56:32Z\", \"level\": \"warn\", \"log\": \"" + logMessage + "2"
                + "\", \"kubernetes\": {\"pod_id\": \"" + executionId + "\", \"labels\": {\"job-name\": \"" + jobName + "\"}}}");

        insertIntoElasticIndex("{\"@timestamp\": \"2022-10-19T13:55:32Z\", \"level\": \"warn\", \"log\": \"" + logMessage + "3"
                + "\", \"kubernetes\": {\"pod_id\": \"" + anotherExecutionId + "\", \"labels\": {\"job-name\": \"" + jobName + "\"}}}");

        waitForElasticsearchIndex();
    }

    @AfterAll
    void dropIndex() throws Exception {
        elastic.getClient().indices().delete(d -> d.index(index));
    }

    private void insertIntoElasticIndex(String input) throws Exception {
        Reader reader = new StringReader(input);

        IndexRequest<JsonData> request = IndexRequest.of(i -> i.index(index).withJson(reader));

        elastic.getClient().index(request);
    }

    private void waitForElasticsearchIndex() {
        Awaitility.await()
                .atMost(2000, TimeUnit.MILLISECONDS)
                .pollDelay(10, TimeUnit.MILLISECONDS)
                .pollInterval(100, TimeUnit.MILLISECONDS)
                .until(() -> elastic.getClient()
                        .search(s -> s.index(index).query(MatchQuery.of(t -> t.field("kubernetes.labels.job-name").query(jobName))._toQuery()),
                                ElasticLogEntry.class)
                        .hits()
                        .total()
                        .value() > 1);
    }

    @Nested
    class getLogs {

        @Test
        void getDefault() throws RuntimeException, IOException {
            String logs = elasticLogService.getLogsForExecution(null, jobName, null, null, null);

            assertEquals(logMessage + logMessage + "2", logs);
        }

        @Test
        void limitResponseSize() throws RuntimeException, IOException {
            String logs = elasticLogService.getLogsForExecution(null, jobName, null, 1, null);

            assertEquals(logMessage + "2", logs);
        }

        @Test
        void filterLogLines() throws RuntimeException, IOException {
            String logs = elasticLogService.getLogsForExecution(null, jobName, ".*2", null, null);

            assertEquals(logMessage + "2", logs);
        }

        @Test
        void includeSourceTimestamps() throws RuntimeException, IOException {
            String logs = elasticLogService.getLogsForExecution(null, jobName, null, null, true);

            assertEquals("2022-10-19T13:55:32Z " + logMessage + "2022-10-19T13:56:32Z " + logMessage + "2", logs);
        }

        @Test
        void anotherExecutionId() throws RuntimeException, IOException {
            String logs = elasticLogService.getLogsForExecution(null, jobName, null, null, null);

            assertEquals(logMessage + "3", logs);
        }
    }
}
