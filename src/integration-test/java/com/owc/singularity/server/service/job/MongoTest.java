package com.owc.singularity.server.service.job;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.owc.singularity.server.service.MongoConnection;
import com.owc.singularity.server.service.SystemPropertyActiveProfileResolver;

@SpringBootTest
@ActiveProfiles(value = "integration-test-local", resolver = SystemPropertyActiveProfileResolver.class)
class MongoTest {

    @Autowired
    MongoConnection mongo;

    @Test
    void connects() throws Exception {
        assertTrue(mongo.testConnection());
    }
}
