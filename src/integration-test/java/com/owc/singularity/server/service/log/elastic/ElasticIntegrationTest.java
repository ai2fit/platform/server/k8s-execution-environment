package com.owc.singularity.server.service.log.elastic;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.owc.singularity.server.service.SystemPropertyActiveProfileResolver;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.transport.endpoints.BooleanResponse;

@SpringBootTest
@ActiveProfiles(value = "integration-test-local", resolver = SystemPropertyActiveProfileResolver.class)
class ElasticIntegrationTest {

    @Autowired
    Elastic elastic;

    @Test
    void connects() throws Exception {
        ElasticsearchClient client = elastic.getClient();

        BooleanResponse booleanResponse = client.ping();
        assertTrue(booleanResponse.value());
    }
}
