package com.owc.singularity.server.service.job.k8s;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.awaitility.Awaitility;
import org.bson.conversions.Bson;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.owc.singularity.server.api.JobsApi;
import com.owc.singularity.server.model.ExecutionContext;
import com.owc.singularity.server.model.Job;
import com.owc.singularity.server.model.JobConfiguration;
import com.owc.singularity.server.model.MountConfiguration;
import com.owc.singularity.server.model.QueryOptions;
import com.owc.singularity.server.model.QueryOptions.Direction;
import com.owc.singularity.server.model.module.ModuleInfo;
import com.owc.singularity.server.service.MongoConnection;
import com.owc.singularity.server.service.SystemPropertyActiveProfileResolver;
import com.owc.singularity.server.service.environment.LocalStorageModuleService;
import com.owc.singularity.server.service.exception.AccessRightException;
import com.owc.singularity.server.service.job.k8s.scheduler.K8sScheduler;

@SpringBootTest
@ActiveProfiles(value = "integration-test-local", resolver = SystemPropertyActiveProfileResolver.class)
public class K8sJobServiceTest {

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class MongoDbTests {

        @Autowired
        MongoConnection mongo;

        @Autowired
        ObjectMapper objectMapper;

        @Autowired
        K8sJobService jobService;

        @MockBean
        K8sScheduler scheduler;


        @Nested
        @TestInstance(TestInstance.Lifecycle.PER_CLASS)
        class Query {

            Job job;
            Job anotherJob;
            Job thirdJob;

            @BeforeAll
            void insertJobs() throws Exception {
                job = new Job().id(UUID.randomUUID().toString())
                        ._configuration(new JobConfiguration().pipelinePath("path"))
                        .name("name")
                        .description("desc")
                        .submittedAt(Instant.now());

                anotherJob = new Job().id(UUID.randomUUID().toString())
                        ._configuration(new JobConfiguration().pipelinePath("path2"))
                        .name("name2")
                        .description("desc2")
                        .submittedAt(Instant.now());

                thirdJob = new Job().id(UUID.randomUUID().toString())
                        ._configuration(new JobConfiguration().pipelinePath("path3"))
                        .name("name3")
                        .description("desc2")
                        .submittedAt(Instant.now());

                assertTrue(mongo.testConnection());

                job.setObjectId(mongo.getJobsCollection().insertOne(job).getInsertedId().asObjectId().getValue());
                anotherJob.setObjectId(mongo.getJobsCollection().insertOne(anotherJob).getInsertedId().asObjectId().getValue());
                thirdJob.setObjectId(mongo.getJobsCollection().insertOne(thirdJob).getInsertedId().asObjectId().getValue());
            }

            @AfterAll
            void dropJobsCollection() throws Exception {
                mongo.getJobsCollection().drop();
            }

            @Test
            void all() throws AccessRightException, IOException {
                Stream<Job> jobs = jobService.all(null);
                List<Job> jobList = jobs.collect(Collectors.toList());

                assertTrue(jobList.contains(job));
                assertTrue(jobList.contains(anotherJob));
                assertTrue(jobList.contains(thirdJob));
            }

            @Nested
            class Filter {

                @Test
                void empty() throws AccessRightException, IOException {
                    Stream<Job> jobs = jobService.filter(null, new QueryOptions());
                    List<Job> jobList = jobs.collect(Collectors.toList());

                    assertTrue(jobList.contains(job));
                    assertTrue(jobList.contains(anotherJob));
                    assertTrue(jobList.contains(thirdJob));
                }

                @Test
                void limitSize() throws AccessRightException, IOException {
                    QueryOptions options = new QueryOptions();
                    options.setSize(1);

                    Stream<Job> jobs = jobService.filter(null, options);
                    List<Job> jobList = jobs.collect(Collectors.toList());

                    assertTrue(jobList.contains(thirdJob));
                    assertFalse(jobList.contains(anotherJob));
                    assertFalse(jobList.contains(job));
                }

                @Test
                void cursor() throws AccessRightException, IOException {
                    QueryOptions options = new QueryOptions();
                    options.setCursor(job.getObjectId().toHexString());

                    Stream<Job> jobs = jobService.filter(null, options);
                    List<Job> jobList = jobs.collect(Collectors.toList());

                    assertFalse(jobList.contains(job));
                    assertTrue(jobList.contains(anotherJob));
                    assertTrue(jobList.contains(thirdJob));
                }

                @Test
                void cursorWithFilter() throws AccessRightException, IOException {
                    QueryOptions options = new QueryOptions();
                    options.setCursor(anotherJob.getObjectId().toHexString());
                    options.setFilter(Arrays.asList("description:desc2"));

                    Stream<Job> jobs = jobService.filter(null, options);
                    List<Job> jobList = jobs.collect(Collectors.toList());

                    assertEquals(1, jobList.size());
                    assertTrue(jobList.contains(thirdJob));
                }

                @Test
                void cursorBackwardsNotSupported() throws AccessRightException, IOException {
                    QueryOptions options = new QueryOptions();
                    options.setCursor(job.getObjectId().toHexString());
                    options.setDirection(Direction.Before);

                    assertThrows(IllegalArgumentException.class, () -> jobService.filter(null, options));
                }

                @Test
                void cursorWithSortCurrentlyNotSupported() throws AccessRightException, IOException {
                    QueryOptions options = new QueryOptions();
                    options.setCursor(job.getObjectId().toHexString());
                    options.setSort(Arrays.asList("null"));

                    assertThrows(IllegalArgumentException.class, () -> jobService.filter(null, options));
                }

                @Test
                void sortWith_IdTiebreaker() throws AccessRightException, IOException {
                    QueryOptions options = new QueryOptions();
                    options.setSort(Arrays.asList("description,desc"));

                    Stream<Job> jobs = jobService.filter(null, options);
                    List<Job> jobList = jobs.collect(Collectors.toList());

                    assertEquals(jobList.get(0), thirdJob);
                    assertEquals(jobList.get(1), anotherJob);
                    assertEquals(jobList.get(2), job);
                }

                @Test
                void multipleSort() throws AccessRightException, IOException {
                    QueryOptions options = new QueryOptions();
                    options.setSort(Arrays.asList("description,desc", "controllerId,desc"));

                    Stream<Job> jobs = jobService.filter(null, options);
                    List<Job> jobList = jobs.collect(Collectors.toList());

                    assertEquals(jobList.get(0), thirdJob);
                    assertEquals(jobList.get(1), anotherJob);
                    assertEquals(jobList.get(2), job);
                }

                @Test
                void throwingSort() throws AccessRightException, IOException {
                    QueryOptions options = new QueryOptions();
                    options.setSort(Arrays.asList("description,asdasd", "controllerId,desc"));

                    assertThrows(IllegalArgumentException.class, () -> jobService.filter(null, options));
                }

                @Test
                void singleFilter() throws AccessRightException, IOException {
                    QueryOptions options = new QueryOptions();
                    options.setFilter(Arrays.asList("controllerId:.*3"));

                    Stream<Job> jobs = jobService.filter(null, options);
                    List<Job> jobList = jobs.collect(Collectors.toList());

                    assertEquals(1, jobList.size());
                    assertTrue(jobList.contains(thirdJob));
                }

                @Test
                void multipleFilters() throws AccessRightException, IOException {
                    QueryOptions options = new QueryOptions();
                    options.setFilter(Arrays.asList("description:desc2", "controllerId:.*3"));

                    Stream<Job> jobs = jobService.filter(null, options);
                    List<Job> jobList = jobs.collect(Collectors.toList());

                    assertEquals(1, jobList.size());
                    assertTrue(jobList.contains(thirdJob));
                }
            }

            @Test
            void findById() throws AccessRightException, IOException {
                Job foundJob = jobService.findById(null, job.getId());
                assertEquals(job, foundJob);

                Job foundAnotherJob = jobService.findById(null, anotherJob.getId());
                assertEquals(anotherJob, foundAnotherJob);
            }
        }

        @Test
        void submit() throws Exception {
            Job submitJob = new Job().id(UUID.randomUUID().toString())
                    ._configuration(new JobConfiguration().pipelinePath("path"))
                    .name("name")
                    .description("desc")
                    .submittedAt(Instant.now());

            JobsApi.JobSubmission submission = new JobsApi.JobSubmission().configuration(submitJob.getConfiguration())
                    .description(submitJob.getDescription())
                    .name(submitJob.getName());

            Job submittedJob = jobService.submit(null, submission);

            Job jobWithSubmittedId = submitJob.id(submittedJob.getId()).submittedAt(submittedJob.getSubmittedAt());
            assertEquals(jobWithSubmittedId, submittedJob);
        }
    }

    @Nested
    class KubernetesTests {

        @MockBean
        MongoConnection mongo;

        @Mock
        MongoCollection<Job> collection;
        @Mock
        FindIterable<Job> findIterable;
        @Mock
        Job document;

        @Autowired
        ObjectMapper objectMapper;

        @MockBean
        LocalStorageModuleService moduleService;

        @Autowired
        K8sJobService jobService;

        @Autowired
        K8sScheduler scheduler;

        Job job = new Job().id(UUID.randomUUID().toString())
                ._configuration(new JobConfiguration().pipelinePath("path"))
                .name("name")
                .description("desc")
                .submittedAt(Instant.now());

        @Test
        void submitFailsWithoutContext() throws Exception {
            JobsApi.JobSubmission submission = new JobsApi.JobSubmission().configuration(job.getConfiguration())
                    .description(job.getDescription())
                    .name(job.getName());

            given(mongo.getJobsCollection()).willReturn(collection);
            given(collection.insertOne(any())).willReturn(null);
            given(collection.find(any(Bson.class))).willReturn(findIterable);
            given(findIterable.first()).willReturn(document);

            assertThrows(RuntimeException.class, () -> jobService.submit(null, submission));
        }

        @Test
        void submitFailsWithoutMountConfiguration() throws Exception {
            JobsApi.JobSubmission submission = new JobsApi.JobSubmission().configuration(job.getConfiguration().context(new ExecutionContext()))
                    .description(job.getDescription())
                    .name(job.getName());

            given(mongo.getJobsCollection()).willReturn(collection);
            given(collection.insertOne(any())).willReturn(null);
            given(collection.find(any(Bson.class))).willReturn(findIterable);
            given(findIterable.first()).willReturn(document);

            assertThrows(RuntimeException.class, () -> jobService.submit(null, submission));
        }

        @Test
        void submit() throws Exception {

            Map<String, String> repositoryMountOptions = new LinkedHashMap<>();
            repositoryMountOptions.put("remote", "https://gitlab.oldworldcomputing.com/test/simple-repository.git");
            repositoryMountOptions.put("auth.username", "PRIVATE-TOKEN");
            repositoryMountOptions.put("auth.password", "VcKRjEvqM453pnvZyemq");

            JobsApi.JobSubmission submission = new JobsApi.JobSubmission()
                    .configuration(job.getConfiguration()
                            .context(new ExecutionContext()
                                    .mounts(Collections.singletonList(new MountConfiguration().path("/repository").options(repositoryMountOptions)))))
                    .description(job.getDescription())
                    .name(job.getName());

            final ArgumentCaptor<Job> jobCaptor = ArgumentCaptor.forClass(Job.class);

            given(mongo.getJobsCollection()).willReturn(collection);
            given(collection.insertOne(any())).willReturn(null);
            given(collection.find(any(Bson.class))).willReturn(findIterable);
            given(findIterable.first()).willReturn(document);

            given(moduleService.all(any())).willReturn(Collections.<ModuleInfo> emptyList().stream());

            jobService.submit(null, submission);

            verify(collection).insertOne(jobCaptor.capture());
            String id = jobCaptor.getValue().getId();

            Optional<com.owc.singularity.job.scheduling.Job> optionalK8sJob = scheduler.getJob(id);
            assertFalse(optionalK8sJob.isEmpty());
            assertEquals(optionalK8sJob.get().getUUID().toString(), id);

            Awaitility.await().atMost(60, TimeUnit.SECONDS).until(() -> scheduler.getJob(id).isPresent());
        }

        @Test
        void stop() throws Exception {
            Map<String, String> repositoryMountOptions = new LinkedHashMap<>();
            repositoryMountOptions.put("remote", "https://gitlab.oldworldcomputing.com/test/simple-repository.git");
            repositoryMountOptions.put("auth.username", "PRIVATE-TOKEN");
            repositoryMountOptions.put("auth.password", "VcKRjEvqM453pnvZyemq");

            JobsApi.JobSubmission submission = new JobsApi.JobSubmission()
                    .configuration(job.getConfiguration()
                            .context(new ExecutionContext()
                                    .mounts(Collections.singletonList(new MountConfiguration().path("/repository").options(repositoryMountOptions)))))
                    .description(job.getDescription())
                    .name(job.getName());

            final ArgumentCaptor<Job> jobCaptor = ArgumentCaptor.forClass(Job.class);

            given(mongo.getJobsCollection()).willReturn(collection);
            given(collection.insertOne(any())).willReturn(null);
            given(collection.find(any(Bson.class))).willReturn(findIterable);
            given(findIterable.first()).willReturn(document);

            jobService.submit(null, submission);

            verify(collection).insertOne(jobCaptor.capture());
            String id = (String) jobCaptor.getValue().getId();

            Optional<com.owc.singularity.job.scheduling.Job> optionalK8sJob = scheduler.getJob(id);
            assertTrue(optionalK8sJob.isPresent());
            assertEquals(optionalK8sJob.get().getUUID().toString(), id);

            jobService.stopById(null, id);
            Optional<io.fabric8.kubernetes.api.model.batch.v1.Job> rawJob = scheduler.getRawJob(id);
            assertTrue(rawJob.isPresent());
            assertTrue(rawJob.get().getSpec().getSuspend());
        }
    }

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class MongoAndKubernetesTests {

        @Autowired
        MongoConnection mongo;

        @Autowired
        ObjectMapper objectMapper;

        @Autowired
        K8sJobService jobService;

        @Autowired
        K8sScheduler scheduler;

        Job job = new Job().id(UUID.randomUUID().toString())
                ._configuration(new JobConfiguration().pipelinePath("path"))
                .name("name")
                .description("desc")
                .submittedAt(Instant.now());

        @AfterAll
        void dropJobsCollection() throws Exception {
            mongo.getJobsCollection().drop();
        }

        @Test
        void submit() throws Exception {
            Map<String, String> repositoryMountOptions = new LinkedHashMap<>();
            repositoryMountOptions.put("remote", "https://gitlab.oldworldcomputing.com/test/simple-repository.git");
            repositoryMountOptions.put("auth.username", "PRIVATE-TOKEN");
            repositoryMountOptions.put("auth.password", "VcKRjEvqM453pnvZyemq");

            JobsApi.JobSubmission submission = new JobsApi.JobSubmission()
                    .configuration(job.getConfiguration()
                            .context(new ExecutionContext()
                                    .mounts(Collections.singletonList(new MountConfiguration().path("/repository").options(repositoryMountOptions)))))
                    .description(job.getDescription())
                    .name(job.getName());

            Job submittedJob = jobService.submit(null, submission);

            Optional<com.owc.singularity.job.scheduling.Job> optionalK8sJob = scheduler.getJob(submittedJob.getId());
            assertTrue(optionalK8sJob.isPresent());
            assertEquals(optionalK8sJob.get().getUUID().toString(), submittedJob.getId());

            Awaitility.await().atMost(60, TimeUnit.SECONDS).until(() -> scheduler.getJob(submittedJob.getId()).isPresent());
        }
    }
}
