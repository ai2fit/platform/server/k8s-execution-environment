<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues page
and verify the issue you're about to submit isn't a duplicate.
--->

## The goal

(What do we want to achieve with this feature? Try to define the who/what/why of the opportunity as a user story. For example, "As a (who), I want (what), so I can (why/value)")


## Proposal

(Add a proposal or ideas on how to solve this problem)

<!-- Optional
## *Current* behavior

(Describe the current behavior of the program. If the goal is currently achievable but could be improved, then describe the current steps to achieve it.)
-->

## *Expected* behavior 

(Describe the expected behavior of the software)

## Documentation

(Describe which documentation should be also revised with new information about this feature. For example, `README.md` file or wiki pages)

## Test cases

(Add a proposal or ideas on how to test if this feature achieves the goal or solves the problem)

<!-- Optional
## Links / References

(Provide some additional links or references)

-->

/label ~enhancement ~"needs approval"
